//
//  BWRootPresenter+MenuOutput.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension RootPresenter: MenuModuleOutput {
    
    func userSelectedHome() {
        view.closeMenu()
        
        guard (currentScreen as? HomeViewController) == nil else {
            return
        }
        
        if let currentScreen = currentScreen {
            router.hideScreen(screenController: currentScreen)
        }
        
        showHomeScreen()
    }
    
    func userSelectedHistory() {
        view.closeMenu()
        
        guard (currentScreen as? HistoryViewController) == nil else {
            return
        }
        
        if let currentScreen = currentScreen {
            router.hideScreen(screenController: currentScreen)
        }
        
        showHistoryScreen()
    }
    
    func userSelectedProjects() {
        view.closeMenu()
        
        guard (currentScreen as? ProjectsViewController) == nil else {
            return
        }
        
        if let currentScreen = currentScreen {
            router.hideScreen(screenController: currentScreen)
        }
        
        showProjectsScreen()
    }
    
    func userSelectedProfiles() {
        view.closeMenu()
        
        guard (currentScreen as? ProfilesViewController) == nil else {
            return
        }
        
        if let currentScreen = currentScreen {
            router.hideScreen(screenController: currentScreen)
        }
        
        showProfilesScreen()
    }
    
    func userSelectedAbout() {
        view.closeMenu()
        
        guard (currentScreen as? AboutViewController) == nil else {
            return
        }
        
        if let currentScreen = currentScreen {
            router.hideScreen(screenController: currentScreen)
        }
        
        showAboutScreen()
    }
    
    func userSelectedReset() {
        view.askToResetAllData(acceptHandler: { 
            print("reset all data")
            self.userSelectedHome()
        }) {
            self.view.closeMenu()
        }
    }
    
}
