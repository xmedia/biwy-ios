//
//  BWCreateProfileBWCreateProfileConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateProfileModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? CreateProfileViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: CreateProfileViewController) {
        let router = CreateProfileRouter()
        
        let presenter = CreateProfilePresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = CreateProfileInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
