//
//  CreateProfileBWCreateProfileViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateProfileViewOutput {
    
    func viewIsReady()
    func didTapCloseButton()
    func didTapCreateButton(jobTitle: String, dayRate: Double?, annualSalary: Double?)
    
}
