//
//  BWCreateMeetingBWCreateMeetingInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateMeetingInteractor: CreateMeetingInteractorInput {
    
    weak var output: CreateMeetingInteractorOutput!
    private let realm = SharedRealm.realm
    
    
    // MARK: - CreateMeetingInteractorInput
    
    func fetchProfiles() {
        guard let realm = realm else {
            output.didFailFetchProfiles()
            return
        }
        
        let profiles = realm.objects(Profile.self)
        output.didSucceedFetchProfiles(Array(profiles))
    }
    
}
