//
//  HistoryBWHistoryPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class HistoryPresenter: HistoryModuleInput, HistoryViewOutput, HistoryInteractorOutput {
    
    weak var view: HistoryViewInput!
    var interactor: HistoryInteractorInput!
    var router: HistoryRouterInput!
    
    
    // MARK: - HistoryModuleInput
    
    func configureModule(menuControls: MenuToggleProtocol) {
        view.configureNavigation(menuControls: menuControls)
    }
    
    
    
    // MARK: - HistoryViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
        interactor.fetchMeetings()
    }
    
    func userDidTapToRemoveMeeting(_ meeting: Meeting) {
        interactor.removeMeeting(meeting)
    }
    
    
    
    // MARK: - HistoryInteractorOutput
    
    func didSucceedMeetingsFetch(meetings: Array<Meeting>) {
        view.populateWithMeetings(meetings)
    }
    
    func didFailMeetingsFetch() {
        // do nothing, show nothing
    }
    
    func didSucceedMeetingRemoval() {
        // do nothing, it was removed
    }
    
    func didFailMeetingRemoval() {
        // TODO: aici tre de facut ceva
    }
    
}
