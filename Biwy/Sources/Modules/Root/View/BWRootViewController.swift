//
//  RootBWRootViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit
import pop

class RootViewController: UIViewController, RootViewInput, POPAnimationDelegate {
    
    var output: RootViewOutput!
    
    @IBOutlet weak var edgePanGestureRecognizer: UIScreenEdgePanGestureRecognizer!
    @IBOutlet weak var panGestureRecognizer: UIPanGestureRecognizer!
    @IBOutlet weak var closeMenuButton: UIButton!
    
    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var menuContainerViewLeading: NSLayoutConstraint!
    
    @IBOutlet weak var screenContainerView: UIView!
    @IBOutlet weak var screenContainerViewLeading: NSLayoutConstraint!
    
    var menuClosed: Bool = true
    var menuContainer: UIView { return menuContainerView }
    var screenContainer: UIView { return screenContainerView }
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - RootViewInput
    
    func activatePanGestureRecognizers() {
        edgePanGestureRecognizer.isEnabled = true
    }
    
    func deactivatePanGestureRecognizers() {
        edgePanGestureRecognizer.isEnabled = false
    }
    
    func openMenu() {
        menuClosed = false
        animate(initialVelocity: 0)
    }
    
    func closeMenu() {
        menuClosed = true
        animate(initialVelocity: 0)
    }
    
    func askToResetAllData(acceptHandler: (() -> ())?, denyHandler: (() -> ())?) {
        let alertMessage = NSLocalizedString("RESET_DIALOG_TITLE",
                                             comment: "Alert message")
        
        let alert = UIAlertController(title: nil,
                                      message: alertMessage,
                                      preferredStyle: .alert)
        
        let confirmActionTitle = NSLocalizedString("RESET_DIALOG_CONFIRM",
                                                   comment: "Confirm button title")
        let confirmAction = UIAlertAction(title: confirmActionTitle,
                                          style: .destructive) { _ in
                                            acceptHandler?()
        }
        alert.addAction(confirmAction)
        
        let denyActionTitle = NSLocalizedString("RESET_DIALOG_DENY",
                                                comment: "Deny button title")
        let denyAction = UIAlertAction(title: denyActionTitle,
                                       style: .cancel) { _ in
                                        denyHandler?()
        }
        alert.addAction(denyAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view)
        viewDragged(translation.x)
        
        gestureRecognizer.setTranslation(CGPoint.zero, in: view)
        if (gestureRecognizer.state == .began) {
            screenContainerViewLeading.pop_removeAllAnimations()
        } else if (gestureRecognizer.state == .ended) {
            let velocity = gestureRecognizer.velocity(in: view)
            menuClosed = velocity.x < 0
            
            animate(initialVelocity: velocity.y)
        }
    }
    
    @IBAction func closeSideMenu() {
        menuClosed = true
        animate(initialVelocity: 0)
    }
    
    
    
    // MARK: - Private Methods
    
    internal func viewDragged(_ delta: CGFloat) {
        screenContainerViewLeading.constant += delta
        
        if screenContainerViewLeading.constant < 0 {
            screenContainerViewLeading.constant = 0
        }
        
        if screenContainerViewLeading.constant > menuContainerView.bounds.width {
            screenContainerViewLeading.constant = menuContainerView.bounds.width
        }
        
        menuContainerViewLeading.constant = -40 + screenContainerViewLeading.constant / menuContainerView.bounds.width * 40
        menuContainerView.alpha = screenContainerViewLeading.constant / 200
    }
    
    internal func animate(initialVelocity: CGFloat) {
        let targetPointX = menuClosed ? 0 : menuContainerView.bounds.width
        if let containerLeadingAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
            containerLeadingAnimation.fromValue = screenContainerViewLeading.constant
            containerLeadingAnimation.toValue = targetPointX
            containerLeadingAnimation.springBounciness = 0
            containerLeadingAnimation.springSpeed = 20
            containerLeadingAnimation.velocity = initialVelocity
            containerLeadingAnimation.animationDidApplyBlock = viewDraggedAnimated
            containerLeadingAnimation.animationDidReachToValueBlock = animationFinished
            screenContainerViewLeading.pop_add(containerLeadingAnimation, forKey: "toggleSideMenu")
        }
        
        closeMenuButton.isHidden = menuClosed
    }
    
    internal func viewDraggedAnimated(_ animation: POPAnimation?) {
        menuContainerViewLeading.constant = -40 + screenContainerViewLeading.constant / menuContainerView.bounds.width * 40
        menuContainerView.alpha = screenContainerViewLeading.constant / 200
    }
    
    internal func animationFinished(_ animation: POPAnimation?) {
        panGestureRecognizer.isEnabled = !menuClosed
    }
    
}
