//
//  HistoryBWHistoryViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol HistoryViewOutput {
    
    func viewIsReady()
    func userDidTapToRemoveMeeting(_ meeting: Meeting)
    
}
