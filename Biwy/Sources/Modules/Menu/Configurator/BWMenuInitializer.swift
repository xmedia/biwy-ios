//
//  BWMenuBWMenuInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class MenuModuleInitializer: NSObject {
    
    @IBOutlet weak var menuViewController: MenuViewController!
    
    override func awakeFromNib() {
        let configurator = MenuModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: menuViewController)
    }
    
}
