//
//  BWProjectsPresenter+CreateProjectOutput.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension ProjectsPresenter: CreateProjectModuleOutput {
    
    func moduleHasFinishedTask(_ moduleInput: CreateProjectModuleInput) {
        interactor.fetchProjects()
        router.hideCreateProjectScreen(moduleInput: moduleInput)
    }
    
}
