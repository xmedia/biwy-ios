//
//  AboutBWAboutViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, AboutViewInput {
    
    var output: AboutViewOutput!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - AboutViewInput
    
    func setupInitialState() {
        
    }
    
}
