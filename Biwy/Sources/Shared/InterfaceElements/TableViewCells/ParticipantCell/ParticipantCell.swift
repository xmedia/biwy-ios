//
//  ParticipantCell.swift
//  Biwy
//
//  Created by Vlad on 11/15/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import pop

class ParticipantCell: UITableViewCell {
    
    private var participantsAmounts: Int = 0 {
        didSet {
            participantsAmountLabel.text = String(describing: participantsAmounts)
            minusButton.isEnabled = participantsAmounts > 0
            
            jobTitleLabel.textColor = participantsAmounts == 0 ? UIColor.white : UIColor.bwGreenColor()
            dayRateLabel.textColor = participantsAmounts == 0 ? UIColor.white : UIColor.bwGreenColor()
            
            if let participantImageAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
                participantImageAnimation.toValue = participantsAmounts == 0 ? -22 : 2
                participantImageLeading.pop_add(participantImageAnimation, forKey: "toggle_participant_icon")
            }
        }
    }
    
    @IBOutlet private weak var participantImageLeading: NSLayoutConstraint!
    @IBOutlet private weak var minusButton: UIButton!
    
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var dayRateLabel: UILabel!
    @IBOutlet weak var participantsAmountLabel: UILabel!
    
    var participantsAmountHasChangedAction: ((ParticipantCell) -> ())?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        minusButton.setImage(UIImage(named: "create_meeting_participant_minus_button_deactivated"), for: .disabled)
    }
    
    
    // MARK: - Actions
    
    @IBAction func minusButtonTapped() {
        participantsAmounts -= 1
        participantsAmountHasChangedAction?(self)
    }
    
    @IBAction func plusButtonTapped() {
        participantsAmounts += 1
        participantsAmountHasChangedAction?(self)
    }
    
}
