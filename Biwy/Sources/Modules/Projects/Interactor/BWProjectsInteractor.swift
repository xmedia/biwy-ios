//
//  BWProjectsBWProjectsInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class ProjectsInteractor: ProjectsInteractorInput {
    
    weak var output: ProjectsInteractorOutput!
    private let realm = SharedRealm.realm
    
    
    // MARK: - ProjectsInteractorInput
    
    func fetchProjects() {
        guard let realm = realm else {
            output.didFailProjectsFetch()
            return
        }
        
        let projects = realm.objects(Project.self)
        output.didSucceedProjectsFetch(projects: Array(projects))
    }
    
    func removeProject(_ project: Project) {
        guard let realm = realm else {
            output.didFailProjectRemoval()
            return
        }
        
        do {
            try realm.write {
                project.meetings.forEach{
                    realm.delete($0)
                }
                
                realm.delete(project)
            }
            output.didSucceedProjectRemoval()
        } catch let error as NSError {
            print(error)
            output.didFailProjectRemoval()
        }
    }
    
}
