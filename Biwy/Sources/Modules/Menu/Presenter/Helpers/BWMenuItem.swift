//
//  MenuItemEnum.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

enum MenuItem: Int {
    
    case Home = 0
    case History = 1
    case Projects = 2
    case Profiles = 3
    case About = 4
    case Reset = 5
    
    func itemTitle() -> String {
        switch self {
        case .Home:
            return NSLocalizedString("MENU_HOME", comment: "Home")
            
        case .History:
            return NSLocalizedString("MENU_HISTORY", comment: "History")
            
        case .Projects:
            return NSLocalizedString("MENU_PROJECTS", comment: "Projects")
            
        case .Profiles:
            return NSLocalizedString("MENU_PROFILES", comment: "Profiles")
            
        case .About:
            return NSLocalizedString("MENU_ABOUT", comment: "About")
            
        case .Reset:
            return NSLocalizedString("MENU_RESET", comment: "Reset")
        }
    }
    
}
