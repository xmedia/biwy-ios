//
//  ProjectsBWProjectsRouterInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

protocol ProjectsRouterInput {
    
    func showCreateProjectScreen(onParentController parentController: UIViewController) -> CreateProjectModuleInput
    func hideCreateProjectScreen(moduleInput: CreateProjectModuleInput)
    
}
