//
//  RGBColor.swift
//  RPlanner
//
//  Created by Vlad on 8/3/16.
//  Copyright © 2016 EverestApps. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
    static func rgbColor(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
    
    static func rgbColorWithWhite(white: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(white: white/255.0, alpha: alpha)
    }
    
}
