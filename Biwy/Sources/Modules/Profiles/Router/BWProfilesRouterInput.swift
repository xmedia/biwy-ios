//
//  ProfilesBWProfilesRouterInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol ProfilesRouterInput {
    
    func showCreateProfileScreen(onParentController parentController: UIViewController) -> CreateProfileModuleInput
    func hideCreateProfileScreen(moduleInput: CreateProfileModuleInput)
    
}
