//
//  HomeBWHomeModuleInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol HomeModuleInput: class {
    
    func configureModule(menuControls: MenuToggleProtocol)
    
}
