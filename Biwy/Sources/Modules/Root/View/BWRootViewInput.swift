//
//  RootBWRootViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

protocol RootViewInput: class {
    
    var menuContainer: UIView { get }
    var screenContainer: UIView { get }
    
    func activatePanGestureRecognizers()
    func deactivatePanGestureRecognizers()
    
    func openMenu()
    func closeMenu()
    
    func askToResetAllData(acceptHandler: (() -> ())?, denyHandler: (() -> ())?)
    
}
