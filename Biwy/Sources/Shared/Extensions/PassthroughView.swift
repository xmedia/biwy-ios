//
//  PassthroughView.swift
//  Flux
//
//  Created by Vlad on 7/12/16.
//  Copyright © 2016 Flux. All rights reserved.
//

import UIKit

class PassthroughView: UIView {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for view in self.subviews {
            if !view.isHidden && view.alpha > 0 &&
                view.isUserInteractionEnabled &&
                view.point(inside: self.convert(point, to: view), with: event) {
                return true
            }
        }
        
        return false
    }
}
