//
//  BWProfilesBWProfilesConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProfilesModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? ProfilesViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: ProfilesViewController) {
        let router = ProfilesRouter()
        
        let presenter = ProfilesPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = ProfilesInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
