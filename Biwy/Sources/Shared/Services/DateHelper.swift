//
//  DateHelper.swift
//  Biwy
//
//  Created by Vlad on 11/11/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

class DateHelper {
    
    static func shortDateString(from date: Date) -> String {
        let shortDateFormatter = DateFormatter()
        shortDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        shortDateFormatter.dateFormat = "dd/MM"
        
        return shortDateFormatter.string(from: date)
    }
    
}
