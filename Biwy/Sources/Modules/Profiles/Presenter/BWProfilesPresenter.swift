//
//  ProfilesBWProfilesPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class ProfilesPresenter: ProfilesModuleInput, ProfilesViewOutput, ProfilesInteractorOutput {
    
    weak var view: ProfilesViewInput!
    var interactor: ProfilesInteractorInput!
    var router: ProfilesRouterInput!
    
    
    // MARK: - ProfilesModuleInput
    
    func configureModule(menuControls: MenuToggleProtocol) {
        view.configureNavigation(menuControls: menuControls)
    }
    
    
    
    // MARK: - ProfilesViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
        interactor.fetchProfiles()
    }
    
    func userDidTapToRemoveProfile(_ profile: Profile) {
        interactor.removeProfile(profile)
    }
    
    func userDidTapToChangeMyProfile(_ myProfile: MyProfile, toProfile profile: Profile) {
        interactor.setMyProfile(myProfile, toProfile: profile)
    }
    
    func didTapAddProfile() {
        showCreateProfileScreen()
    }
    
    
    
    // MARK: - ProfilesInteractorOutput
    
    func didSucceedProfilesFetch(profiles: Array<Profile>, myProfile: MyProfile) {
        view.populateWithProfiles(profiles, myProfile: myProfile)
    }
    
    func didFailProfilesFetch() {
        // do nothing, show nothing
    }
    
    func didSucceedProfileRemoval() {
        // do nothing, it was removed
    }
    
    func didFailProfileRemoval() {
        // TODO: aici treb de facut ceva
    }
    
    func didSucceedSetMyProfile(_ myProfile: MyProfile) {
        view.updateMyProfile(myProfile)
    }
    
    func didFailSetMyProfile() {
        // TODO: ar trebui de facut ceva
    }
    
    
    
    // MARK: - Private Methods
    
    internal func showCreateProfileScreen() {
        let viewController = view as! UIViewController
        let createProfileModule = router.showCreateProfileScreen(onParentController: viewController)
        createProfileModule.configureModule(delegate: self)
    }
    
}
