//
//  TimeHelper.swift
//  Biwy
//
//  Created by Vlad on 11/10/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

class TimeHelper {
    
    static let kWorkHoursPerDay: Double = 8
    static let kMinutesPerHour: Double = 60
    static let kSecondsPerMinute: Double = 60
    
    static let kSecondsPerHour = kMinutesPerHour * kSecondsPerMinute
    static let kSecondsPerDay = kWorkHoursPerDay * kSecondsPerHour
    
    static func timeString(timeInSeconds: Double) -> String {
        let days: Int = Int(timeInSeconds / kSecondsPerDay)
        let hours: Int = Int((timeInSeconds - Double(days) * kSecondsPerDay) / kSecondsPerHour)
        let minutes: Int = Int((timeInSeconds - Double(days) * kSecondsPerDay - Double(hours) * kSecondsPerHour) / kSecondsPerMinute)
        let seconds: Int = Int(timeInSeconds - Double(days) * kSecondsPerDay - Double(hours) * kSecondsPerHour - Double(minutes) * kSecondsPerMinute)
        
        if days > 0 {
            return String(format: "%02d%@%02d%@%02d", days, "d", hours, "h", minutes)
        } else if hours > 0 {
            return String(format: "%02d%@%02d", hours, "h", minutes)
        } else {
            return String(format: "%02d%@%02d", minutes, "m", seconds)
        }
    }
    
    static func shortTimeString(timeInSeconds: Double) -> String {
        let hours: Int = Int(timeInSeconds / kSecondsPerHour)
        let minutes: Int = Int((timeInSeconds - Double(hours) * kSecondsPerHour) / kSecondsPerMinute)
        
        return String(format: "%d%@%02d", hours, "h", minutes)
    }
    
}
