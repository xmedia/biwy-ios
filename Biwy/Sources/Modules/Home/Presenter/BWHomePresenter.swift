//
//  HomeBWHomePresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class HomePresenter: HomeModuleInput, HomeViewOutput, HomeInteractorOutput {
    
    weak var view: HomeViewInput!
    var interactor: HomeInteractorInput!
    var router: HomeRouterInput!
    
    var menuControls: MenuToggleProtocol!
    var navigationController: UINavigationController!
    
    
    // MARK: - HomeModuleInput
    
    func configureModule(menuControls: MenuToggleProtocol) {
        self.menuControls = menuControls
    }
    
    
    
    // MARK: - HomeViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
        view.configureNavigation(menuControls: menuControls)
        
        interactor.fetchCurrentReport()
    }
    
    func viewDidAppear() {
        menuControls.activatePanGestureRecognizer()
    }
    
    func didTapMoreButton() {
        
    }
    
    func didTapCreateMeetingButton() {
        showCreateMeetingScreen()
    }
    
    
    
    // MARK: - HomeInteractorOutput
    
    func didSucceedCurrentReportFetch(totalTime: Double, meetings: Array<Meeting>, projects: Int, individualCost: Double) {
        let timeString = TimeHelper.timeString(timeInSeconds: totalTime)
        
        let currency = NSLocalizedString("CURRENCY", comment: "Currency")
        let individualCostString = String(format: "%.0f%@", round(individualCost), currency)
        
        view.showReport(totalTime: timeString, meetings: meetings, projects: projects, individualCost: individualCostString)
    }
    
    func didFailCurrentReportFetch() {
        // do nothing, show nothing
    }
    
    
    
    // MARK: - Private Methods
    
    func showCreateMeetingScreen() {
        menuControls.deactivatePanGestureRecognizer()
        
        let viewController = view as! UIViewController
        router.showCreateMeetingScreen(sourceController: viewController)
    }
    
}
