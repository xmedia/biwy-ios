//
//  BWHistoryBWHistoryInteractorInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol HistoryInteractorInput {
    
    func fetchMeetings()
    func removeMeeting(_ meeting: Meeting)
    
}
