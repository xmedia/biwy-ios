//
//  BWAboutBWAboutConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class AboutModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? AboutViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: AboutViewController) {
        let router = AboutRouter()
        
        let presenter = AboutPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = AboutInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
