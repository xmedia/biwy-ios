//
//  CreateMeetingBWCreateMeetingRouterInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol CreateMeetingRouterInput {
    
    func goBack(viewController: UIViewController)
    
    func showCreateProjectScreen(onParentController parentController: UIViewController) -> CreateProjectModuleInput
    func hideCreateProjectScreen(moduleInput: CreateProjectModuleInput)
    
    func showCreateProfileScreen(onParentController parentController: UIViewController) -> CreateProfileModuleInput
    func hideCreateProfileScreen(moduleInput: CreateProfileModuleInput)
    
}
