//
//  CreateProjectBWCreateProjectPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateProjectPresenter: CreateProjectModuleInput, CreateProjectViewOutput, CreateProjectInteractorOutput {
    
    weak var view: CreateProjectViewInput!
    var interactor: CreateProjectInteractorInput!
    var router: CreateProjectRouterInput!
    
    private weak var output: CreateProjectModuleOutput?
    
    
    // MARK: - CreateProjectModuleInput
    
    func configureModule(delegate: CreateProjectModuleOutput) {
        output = delegate
    }
    
    
    
    // MARK: - CreateProjectViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
    }
    
    func didTapCloseButton() {
        output?.moduleHasFinishedTask(self)
    }
    
    func canCreateProjectWithName(_ projectName: String) -> Bool {
        return interactor.isProjectNameUnique(projectName)
    }
    
    func didTapCreateButton(projectName: String) {
        interactor.createProject(name: projectName)
    }
    
    
    
    // MARK: - CreateProjectInteractorOutput
    
    func didSucceedCreateProject() {
        output?.moduleHasFinishedTask(self)
    }
    
    func didFailCreateProject() {
        // TODO: aici tre de facut ceva
    }
    
}
