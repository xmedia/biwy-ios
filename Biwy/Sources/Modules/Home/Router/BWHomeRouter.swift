//
//  HomeBWHomeRouter.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class HomeRouter: HomeRouterInput {
    
    func showHistoryScreen(onParentController parentController: UIViewController, containerView: UIView) {
        let historyScreen = UIStoryboard(name: "History", bundle: nil).instantiateInitialViewController() as! HistoryViewController
        
        historyScreen.willMove(toParentViewController: parentController)
        historyScreen.view.frame = containerView.bounds
        containerView.addSubview(historyScreen.view)
        parentController.addChildViewController(historyScreen)
        historyScreen.didMove(toParentViewController: parentController)
    }
    
    func showCreateMeetingScreen(sourceController: UIViewController) {
        let createMeetingScreen = UIStoryboard(name: "CreateMeeting", bundle: nil).instantiateInitialViewController() as! CreateMeetingViewController
        
        sourceController.navigationController?.pushViewController(createMeetingScreen, animated: true)
    }
    
}
