//
//  ProfilesBWProfilesViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol ProfilesViewInput: class {
    
    func configureNavigation(menuControls: MenuToggleProtocol)
    func setupInitialState()
    func populateWithProfiles(_ profiles: Array<Profile>, myProfile: MyProfile)
    func updateMyProfile(_ myProfile: MyProfile)
    
}
