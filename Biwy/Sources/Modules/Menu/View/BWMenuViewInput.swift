//
//  MenuBWMenuViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol MenuViewInput: class {
    
    func populateMenu(items: Array<MenuItem>)
    
}
