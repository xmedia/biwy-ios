//
//  BWHistoryBWHistoryInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class HistoryInteractor: HistoryInteractorInput {
    
    weak var output: HistoryInteractorOutput!
    private let realm = SharedRealm.realm
    
    
    // MARK: - HistoryInteractorInput
    
    func fetchMeetings() {
        guard let realm = realm else {
            output.didFailMeetingsFetch()
            return
        }
        
        let meetings = realm.objects(Meeting.self).sorted(byProperty: "date", ascending: false)
        output.didSucceedMeetingsFetch(meetings: Array(meetings))
    }
    
    func removeMeeting(_ meeting: Meeting) {
        guard let realm = realm else {
            output.didFailMeetingRemoval()
            return
        }
        
        do {
            try realm.write {
                realm.delete(meeting)
            }
            output.didSucceedMeetingRemoval()
        } catch let error as NSError {
            print(error)
            output.didFailMeetingRemoval()
        }
    }
    
}
