//
//  CreateMeetingBWCreateMeetingRouter.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateMeetingRouter: CreateMeetingRouterInput {
    
    func goBack(viewController: UIViewController) {
        _ = viewController.navigationController?.popViewController(animated: true)
    }
    
    func showCreateProjectScreen(onParentController parentController: UIViewController) -> CreateProjectModuleInput {
        let createProjectScreen = UIStoryboard(name: "CreateProject", bundle: nil).instantiateInitialViewController() as! CreateProjectViewController
        
        createProjectScreen.modalPresentationStyle = .fullScreen
        parentController.present(createProjectScreen, animated: false)
        
        return createProjectScreen.output as! CreateProjectModuleInput
    }
    
    func hideCreateProjectScreen(moduleInput: CreateProjectModuleInput) {
        let presenter = moduleInput as! CreateProjectPresenter
        let controller = presenter.view as! CreateProjectViewController
        
        controller.modalTransitionStyle = .coverVertical
        controller.dismiss(animated: false)
    }
    
    func showCreateProfileScreen(onParentController parentController: UIViewController) -> CreateProfileModuleInput {
        let createProfileScreen = UIStoryboard(name: "CreateProfile", bundle: nil).instantiateInitialViewController() as! CreateProfileViewController
        
        createProfileScreen.modalPresentationStyle = .fullScreen
        parentController.present(createProfileScreen, animated: false)
        
        return createProfileScreen.output as! CreateProfileModuleInput
    }
    
    func hideCreateProfileScreen(moduleInput: CreateProfileModuleInput) {
        let presenter = moduleInput as! CreateProfilePresenter
        let controller = presenter.view as! CreateProfileViewController
        
        controller.modalTransitionStyle = .coverVertical
        controller.dismiss(animated: false)
    }
    
}
