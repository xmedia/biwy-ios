//
//  BWHistoryBWHistoryInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class HistoryModuleInitializer: NSObject {
    
    @IBOutlet weak var historyViewController: HistoryViewController!
    
    override func awakeFromNib() {
        let configurator = HistoryModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: historyViewController)
    }
    
}
