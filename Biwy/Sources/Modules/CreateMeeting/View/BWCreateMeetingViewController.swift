//
//  CreateMeetingBWCreateMeetingViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateMeetingViewController: UIViewController, CreateMeetingViewInput {
    
    var output: CreateMeetingViewOutput!
    
    internal var participants: Array<Profile>?
    @IBOutlet private weak var screenTitleLabel: UILabel!
    @IBOutlet private weak var selectProjectLabel: UILabel!
    @IBOutlet private weak var participantsTable: UITableView!
    @IBOutlet private weak var participantsLabel: UILabel!
    @IBOutlet private weak var screenDescriptionLabel: UILabel!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        enableNavigationControllerInteractivePopGesture()
    }
    
    
    
    // MARK: - CreateMeetingViewInput
    
    func setupInitialState() {
        localize()
        registerTableCustomCells()
    }
    
    func populateWithProfiles(_ profiles: Array<Profile>) {
        participants = profiles
        participantsTable.reloadData()
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func backButtonTapped() {
        output.didTapBackButton()
    }
    
    @IBAction func selectProjectButtonTapped() {
        output.didTapSelectProject()
    }
    
    @IBAction func addProjectButtonTapped() {
        output.didTapAddProject()
    }
    
    @IBAction func startMeetingButtonTapped() {
        output.didTapStartMeeting()
    }
    
    internal func participantsAmountHasChanged(cell: ParticipantCell) {
        
    }
    
    internal func addProfileTapped() {
        output.didTapAddProfile()
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        screenTitleLabel.text = NSLocalizedString("CREATE_MEETING_TITLE", comment: "New meeting")
        selectProjectLabel.text = NSLocalizedString("CREATE_MEETING_SELECT_PROJECT", comment: "Select a project")
        screenDescriptionLabel.text = NSLocalizedString("CREATE_MEETING_START", comment: "Start")
        
        let participantsString = NSLocalizedString("CREATE_MEETING_PARTICIPANT_PLURAL", comment: "Participants")
        participantsLabel.text = String(format: "%d %@", 0, participantsString)
    }
    
    private func registerTableCustomCells() {
        let participantCell = UINib(nibName: "ParticipantCell", bundle: nil)
        participantsTable.register(participantCell, forCellReuseIdentifier: "ParticipantCell")
        
        let addElementCell = UINib(nibName: "AddElementCell", bundle: nil)
        participantsTable.register(addElementCell, forCellReuseIdentifier: "AddElementCell")
    }
    
    internal func enableNavigationControllerInteractivePopGesture() {
        navigationController?.interactivePopGestureRecognizer!.isEnabled = true
    }
    
    
}
