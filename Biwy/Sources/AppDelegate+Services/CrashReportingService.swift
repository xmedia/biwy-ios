//
//  CrashReportingService.swift
//  Biwy
//
//  Created by Vlad on 11/16/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

class CrashReportingService: NSObject, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        Fabric.with([Crashlytics.self])
        
        return true
    }
    
}
