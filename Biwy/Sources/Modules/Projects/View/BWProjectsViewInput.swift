//
//  ProjectsBWProjectsViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol ProjectsViewInput: class {
    
    func configureNavigation(menuControls: MenuToggleProtocol)
    func setupInitialState()
    func populateWithProjects(_ projects: Array<Project>)
    
}
