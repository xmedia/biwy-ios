//
//  Employee.swift
//  Biwy
//
//  Created by Vlad on 11/3/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class Profile: Object {
    
    static private let kWorkHoursPerDay: Double = 8
    static private let kMinutesPerHours: Double = 60
    static private let kSecondsPerMinute: Double = 60
    
    //-- Data
    dynamic var id = NSUUID().uuidString
    dynamic var jobTitle = ""
    dynamic var dayRate: Double = 0
    
    //-- Relationships
    let meetings = LinkingObjects(fromType: Meeting.self, property: "participants")
    
    
    
    // MARK: - Realm Methods
    
    override static func indexedProperties() -> [String] {
        return ["jobTitle"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    // MARK: - Public Methods
    
    func pricePerSecond() -> Double {
        return dayRate /
            type(of: self).kWorkHoursPerDay /
            type(of: self).kMinutesPerHours /
            type(of: self).kSecondsPerMinute
    }
    
    func pricePerTime(inSeconds seconds: Double) -> Double {
        return pricePerSecond() * seconds
    }
    
}
