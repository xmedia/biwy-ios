//
//  Project.swift
//  Biwy
//
//  Created by Vlad on 11/3/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class Project: Object {
    
    //-- Data
    dynamic var name = ""
    
    //-- Relationships
    let meetings = List<Meeting>()
    
    
    
    // MARK: - Realm Methods
    
    override static func indexedProperties() -> [String] {
        return ["name"]
    }
    
    override static func primaryKey() -> String? {
        return "name"
    }
    
    
    
    // MARK: - Public Methods
    
    func totalCost() -> Double {
        return meetings.reduce(0.0) {
            $0 + $1.totalCost()
        }
    }
    
    func totalCostString() -> String {
        return String(format: "%.2f", totalCost())
    }
    
}
