//
//  BWMenuViewTableCell.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class MenuTableCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    
    // MARK: - Cell Methods
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        titleLabel.font = selected ? activeFont() : inactiveFont()
        titleLabel.textColor = selected ? activeColor() : inactiveColor()
    }
    
    
    
    // MARK: - Private Methods
    
    private func activeFont() -> UIFont {
        return UIFont(name: "HelveticaNeueLTStd-MdCn", size: 19)!
    }
    
    private func inactiveFont() -> UIFont {
        return UIFont(name: "HelveticaNeueLTStd-LtCn", size: 19)!
    }
    
    private func activeColor() -> UIColor {
        return UIColor.white
    }
    
    private func inactiveColor() -> UIColor {
        return UIColor.bwLightGrayColor()
    }
    
}
