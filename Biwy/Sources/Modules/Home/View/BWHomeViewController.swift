//
//  HomeBWHomeViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, HomeViewInput, UIGestureRecognizerDelegate {
    
    var output: HomeViewOutput!
    
    @IBOutlet private weak var navigationBar: NavigationBar!
    @IBOutlet private weak var totalTimeDescriptionLabel: UILabel!
    @IBOutlet private weak var totalTimeLabel: UILabel!
    @IBOutlet private weak var meetingsAmountDescriptionLabel: UILabel!
    @IBOutlet private weak var meetingsAmountLabel: UILabel!
    @IBOutlet private weak var projectsAmountDescriptionLabel: UILabel!
    @IBOutlet private weak var projectsAmountLabel: UILabel!
    @IBOutlet private weak var individualCostDescriptionLabel: UILabel!
    @IBOutlet private weak var individualCostLabel: UILabel!
    @IBOutlet private weak var meetingsTable: UITableView!
    internal var meetings: Array<Meeting>?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        disableNavigationControllerInteractivePopGesture()
        output.viewDidAppear()
    }
    
    
    
    // MARK: - HomeViewInput
    
    func setupInitialState() {
        localize()
        registerTableCustomCell()
        addNavigationControllerInteractivePopGestureDelegate()
    }
    
    func configureNavigation(menuControls: MenuToggleProtocol) {
        navigationBar.menuControls = menuControls
        navigationBar.showOnlyMenuButton = true
    }
    
    func showReport(totalTime: String, meetings: Array<Meeting>, projects: Int, individualCost: String) {
        self.meetings = meetings
        totalTimeLabel.text = totalTime
        
        meetingsAmountLabel.text = String(format: "%02d", meetings.count)
        meetingsAmountDescriptionLabel.text = meetings.count == 1 ?
            NSLocalizedString("HOME_MEETINGS_AMOUNT_DESCRIPTION_SINGULAR", comment: "Meetings") :
            NSLocalizedString("HOME_MEETINGS_AMOUNT_DESCRIPTION_PLURAL", comment: "Meetings")
        
        projectsAmountLabel.text = String(format: "%02d", projects)
        projectsAmountDescriptionLabel.text = projects == 1 ?
            NSLocalizedString("HOME_PROJECTS_AMOUNT_DESCRIPTION_SINGULAR", comment: "Projects") :
            NSLocalizedString("HOME_PROJECTS_AMOUNT_DESCRIPTION_PLURAL", comment: "Projects")
        
        individualCostLabel.text = individualCost
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func moreButtonTapped() {
        output.didTapMoreButton()
    }
    
    @IBAction func createMeetingButtonTapped() {
        output.didTapCreateMeetingButton()
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        totalTimeDescriptionLabel.text = NSLocalizedString("HOME_TOTAL_AMOUNT_DESCRIPTION",
                                                           comment: "Total amount")
        individualCostDescriptionLabel.text = NSLocalizedString("HOME_INDIVIDUAL_COST_DESCRIPTION",
                                                                comment: "Individual cost")
    }
    
    private func registerTableCustomCell() {
        let nib = UINib(nibName: "TitleSubtitleInfoCell", bundle: nil)
        meetingsTable.register(nib, forCellReuseIdentifier: "TitleSubtitleInfoCell")
    }
    
    internal func addNavigationControllerInteractivePopGestureDelegate() {
        navigationController?.interactivePopGestureRecognizer!.delegate = self
    }
    
    internal func disableNavigationControllerInteractivePopGesture() {
        navigationController?.interactivePopGestureRecognizer!.isEnabled = false
    }
    
    
    
    // MARK: - UIGestureRecognizerDelegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return navigationController!.viewControllers.count > 1 ? true : false
    }
    
}
