//
//  BWRootBWRootConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class RootModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? RootViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: RootViewController) {
        let router = RootRouter()
        
        let presenter = RootPresenter()
        presenter.view = viewController
        presenter.router = router
        
        viewController.output = presenter
    }
    
}
