//
//  MenuBWMenuViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol MenuViewOutput {
    
    func viewIsReady()
    func userSelectedMenuItem(_ item: MenuItem)
    
}
