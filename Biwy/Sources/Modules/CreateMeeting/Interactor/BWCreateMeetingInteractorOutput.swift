//
//  BWCreateMeetingBWCreateMeetingInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol CreateMeetingInteractorOutput: class {
    
    func didSucceedFetchProfiles(_ profiles: Array<Profile>)
    func didFailFetchProfiles()
    
}
