//
//  ProjectsBWProjectsViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol ProjectsViewOutput {
    
    func viewIsReady()
    func userDidTapToRemoveProject(_ project: Project)
    func didTapAddProject()
    
}
