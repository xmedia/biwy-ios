//
//  BWCreateProfileBWCreateProfileInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateProfileModuleInitializer: NSObject {
    
    @IBOutlet weak var createProfileViewController: CreateProfileViewController!
    
    override func awakeFromNib() {
        let configurator = CreateProfileModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: createProfileViewController)
    }
    
}
