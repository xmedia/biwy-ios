//
//  DataManipulation.swift
//  Biwy
//
//  Created by Vlad on 11/4/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class DataManipulation {
    
    let sharedRealm = try! Realm()
    
    func fillDB(realm: Realm?) {
        let localRealm = realm != nil ? realm! : self.sharedRealm
        
        let director = Profile(value: [NSUUID().uuidString, "Director", 35])
        let designer = Profile(value: [NSUUID().uuidString, "Designer", 15])
        let iosDeveloper1 = Profile(value: [NSUUID().uuidString, "iOS Developer", 25,])
        let iosDeveloper2 = Profile(value: [NSUUID().uuidString, "iOS Developer", 20,])
        let androidDeveloper = Profile(value: [NSUUID().uuidString, "Android Developer", 25])
        
        let myJob = MyProfile(value: [iosDeveloper1])
        
        let meeting1 = Meeting(value: [NSUUID().uuidString, NSDate(), 2390, [director, iosDeveloper1, androidDeveloper]])
        let meeting2 = Meeting(value: [NSUUID().uuidString, NSDate(), 2101, [director, iosDeveloper1, iosDeveloper2]])
        let meeting3 = Meeting(value: [NSUUID().uuidString, NSDate(), 1000, [director, iosDeveloper1, iosDeveloper1]])
        let meeting4 = Meeting(value: [NSUUID().uuidString, NSDate(), 3800, [director, designer, iosDeveloper1]])
        
        let meeting5 = Meeting(value: [NSUUID().uuidString, NSDate(), 2931, [director, designer, designer]])
        let meeting6 = Meeting(value: [NSUUID().uuidString, NSDate(), 4372, [director, designer, iosDeveloper1, androidDeveloper]])
        
        let meeting7 = Meeting(value: [NSUUID().uuidString, NSDate(), 4372, [director, designer, iosDeveloper1, androidDeveloper]])
        
        let skeegle = Project(value: ["Skeegle", [meeting1, meeting2]])
        skeegle.meetings.append(meeting3)
        skeegle.meetings.append(meeting4)
        
        let gkeep = Project(value: ["G-Keep", [meeting5, meeting6]])
        let biwy = Project(value: ["Biwy", [meeting7]])
        
        try! localRealm.write {
            localRealm.add(director)
            localRealm.add(designer)
            localRealm.add(iosDeveloper1)
            localRealm.add(iosDeveloper2)
            localRealm.add(androidDeveloper)
            
            localRealm.add(myJob)
            
            localRealm.add(meeting1)
            localRealm.add(meeting2)
            localRealm.add(meeting3)
            localRealm.add(meeting4)
            localRealm.add(meeting5)
            localRealm.add(meeting6)
            
            localRealm.add(skeegle)
            localRealm.add(gkeep)
            localRealm.add(biwy)
        }
    }
    
    func printProfiles() {
        let profiles = sharedRealm.objects(Profile.self)
        print("Profiles: ", profiles.count)
        profiles.forEach {
            print($0.jobTitle, $0.dayRate)
        }
    }
    
}
