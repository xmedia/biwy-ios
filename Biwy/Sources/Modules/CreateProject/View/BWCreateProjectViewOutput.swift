//
//  CreateProjectBWCreateProjectViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateProjectViewOutput {
    
    func viewIsReady()
    func didTapCloseButton()
    func canCreateProjectWithName(_ projectName: String) -> Bool
    func didTapCreateButton(projectName: String)
    
}
