//
//  RootBWRootRouterInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol RootRouterInput {

    func showSideMenu(onParentController parentController: UIViewController, containerView: UIView) -> MenuModuleInput
    
    func showHomeScreen(onParentController parentController: UIViewController, containerView: UIView) -> HomeModuleInput
    func showHistoryScreen(onParentController parentController: UIViewController, containerView: UIView) -> HistoryModuleInput
    func showProjectsScreen(onParentController parentController: UIViewController, containerView: UIView) -> ProjectsModuleInput
    func showProfilesScreen(onParentController parentController: UIViewController, containerView: UIView) -> ProfilesModuleInput
    func showAboutScreen(onParentController parentController: UIViewController, containerView: UIView) -> AboutModuleInput

    func hideScreen(screenController: UIViewController)
    
}
