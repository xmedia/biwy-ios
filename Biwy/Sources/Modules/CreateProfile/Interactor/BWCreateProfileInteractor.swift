//
//  BWCreateProfileBWCreateProfileInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateProfileInteractor: CreateProfileInteractorInput {
    
    weak var output: CreateProfileInteractorOutput!
    private let realm = SharedRealm.realm
    
    // MARK: - CreateProfileInteractorInput
    
    func createProfile(jobTitle: String, dayRate: Double) {
        guard let realm = realm else {
            output.didFailCreateProfile()
            return
        }
        
        let profile = Profile(value: [NSUUID().uuidString, jobTitle, dayRate])
        do {
            try realm.write {
                realm.add(profile)
            }
            output.didSucceedCreateProfile()
        } catch let error as NSError {
            print(error)
            output.didFailCreateProfile()
        }
    }
    
}
