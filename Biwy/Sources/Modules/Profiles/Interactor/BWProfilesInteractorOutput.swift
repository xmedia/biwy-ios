//
//  BWProfilesBWProfilesInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol ProfilesInteractorOutput: class {
    
    func didSucceedProfilesFetch(profiles: Array<Profile>, myProfile: MyProfile)
    func didFailProfilesFetch()
    
    func didSucceedProfileRemoval()
    func didFailProfileRemoval()
    
    func didSucceedSetMyProfile(_ myProfile: MyProfile)
    func didFailSetMyProfile()
    
}
