//
//  BWCreateProjectBWCreateProjectConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateProjectModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? CreateProjectViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: CreateProjectViewController) {
        let router = CreateProjectRouter()
        
        let presenter = CreateProjectPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = CreateProjectInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
