//
//  BWHistoryBWHistoryInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol HistoryInteractorOutput: class {
    
    func didSucceedMeetingsFetch(meetings: Array<Meeting>)
    func didFailMeetingsFetch()
    
    func didSucceedMeetingRemoval()
    func didFailMeetingRemoval()
    
}
