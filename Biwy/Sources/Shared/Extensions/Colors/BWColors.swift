//
//  CBColors.swift
//  Citybiker
//
//  Created by Vlad on 9/9/16.
//  Copyright © 2016 Citybiker. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
    
    static func bwLightGrayColor() -> UIColor {
        return UIColor.rgbColor(red: 115, green: 124, blue: 148, alpha: 1)
    }
    
    static func bwDarkGrayColor() -> UIColor {
        return UIColor.rgbColor(red: 46, green: 56, blue: 81, alpha: 1)
    }
    
    static func bwGreenColor() -> UIColor {
        return UIColor.rgbColor(red: 99, green: 167, blue: 48, alpha: 1)
    }
    
}
