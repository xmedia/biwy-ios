//
//  SharedRealm.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class SharedRealm {
    
    static var realm: Realm? {
        do {
            return try Realm()
        } catch let error as NSError {
            print(error)
            return nil
        }
    }
    
}
