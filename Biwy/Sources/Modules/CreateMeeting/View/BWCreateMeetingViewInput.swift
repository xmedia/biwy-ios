//
//  CreateMeetingBWCreateMeetingViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateMeetingViewInput: class {
    
    func setupInitialState()
    func populateWithProfiles(_ profiles: Array<Profile>)
    
}
