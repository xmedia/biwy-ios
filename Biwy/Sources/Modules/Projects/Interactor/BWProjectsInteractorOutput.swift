//
//  BWProjectsBWProjectsInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol ProjectsInteractorOutput: class {
    
    func didSucceedProjectsFetch(projects: Array<Project>)
    func didFailProjectsFetch()
    
    func didSucceedProjectRemoval()
    func didFailProjectRemoval()
    
}
