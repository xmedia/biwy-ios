//
//  BWAboutBWAboutInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class AboutModuleInitializer: NSObject {
    
    @IBOutlet weak var aboutViewController: AboutViewController!
    
    override func awakeFromNib() {
        let configurator = AboutModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: aboutViewController)
    }
    
}
