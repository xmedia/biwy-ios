//
//  BWCreateMeetingBWCreateMeetingConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateMeetingModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? CreateMeetingViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: CreateMeetingViewController) {
        let router = CreateMeetingRouter()
        
        let presenter = CreateMeetingPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = CreateMeetingInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
