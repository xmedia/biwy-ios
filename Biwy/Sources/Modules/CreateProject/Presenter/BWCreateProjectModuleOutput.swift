//
//  BWCreateProjectModuleOutput.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateProjectModuleOutput: class {
    
    func moduleHasFinishedTask(_ moduleInput: CreateProjectModuleInput)
    
}
