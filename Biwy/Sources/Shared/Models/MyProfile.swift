//
//  MyJob.swift
//  Biwy
//
//  Created by Vlad on 11/4/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class MyProfile: Object {
    
    //-- Data
    dynamic var profile: Profile?
    
}
