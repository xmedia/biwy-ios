//
//  MenuBWMenuViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, MenuViewInput {
    
    var output: MenuViewOutput!
    
    @IBOutlet internal weak var menuTable: UITableView!
    internal var menuItems: Array<MenuItem>? = nil
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - MenuViewInput
    
    func populateMenu(items: Array<MenuItem>) {
        menuItems = items
        menuTable.reloadData()
        
        menuTable.selectRow(at: IndexPath(row: MenuItem.Home.rawValue, section: 0),
                            animated: false,
                            scrollPosition: .none)
    }
    
}
