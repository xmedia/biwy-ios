//
//  AboutBWAboutPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class AboutPresenter: AboutModuleInput, AboutViewOutput, AboutInteractorOutput {
	
    weak var view: AboutViewInput!
    var interactor: AboutInteractorInput!
    var router: AboutRouterInput!


    // MARK: - AboutViewOutput

    func viewIsReady() {
        
    }

}
