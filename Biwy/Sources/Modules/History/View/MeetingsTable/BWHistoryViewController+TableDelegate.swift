//
//  BWHistoryViewController+TableDelegate.swift
//  Biwy
//
//  Created by Vlad on 11/11/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension HistoryViewController: UITableViewDataSource {
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let rows = meetings?.count ?? 0
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleSubtitleInfoCell") as! TitleSubtitleInfoCell
        configureCell(cell, withMeeting: meetings![indexPath.row])
        
        return cell
    }
    
    
    
    // MARK: - Private Methods
    
    private func configureCell(_ cell: TitleSubtitleInfoCell, withMeeting meeting: Meeting) {
        cell.setEditMode(editMode: navigationBar.editMode, animated: false)
        cell.deleteButtonAction = deleteButtonTapped
        
        cell.titleLabel.text = String(format: "%@ - %@",
                                      DateHelper.shortDateString(from: meeting.date as Date),
                                      TimeHelper.shortTimeString(timeInSeconds: meeting.duration))
        
        let participantsString = meeting.participants.count == 1 ?
            NSLocalizedString("HOME_PARTICIPANTS_SINGULAR", comment: "Participant") :
            NSLocalizedString("HOME_PARTICIPANTS_PLURAL", comment: "Participants")
        cell.subtitleLabel.text = String(format: "%@ - %d %@", meeting.project!.name.uppercased(), meeting.participants.count, participantsString)
        
        let currency = NSLocalizedString("CURRENCY", comment: "Currency")
        cell.infoLabel.text = String(format: "%.2f%@", meeting.totalCost(), currency)
    }
    
}
