//
//  BWProjectsViewController+TableDelegate.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

extension ProjectsViewController: UITableViewDataSource {
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let additionalSections = navigationBar.editMode ? 0 : 1
        return 1 + additionalSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let firstSectionRows = projects?.count ?? 0
        return section == 0 ? firstSectionRows : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "TitleSubtitleInfoCell") as! TitleSubtitleInfoCell
            configureCell(cell as! TitleSubtitleInfoCell, withProject: projects![indexPath.row])
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "AddElementCell") as! AddElementCell
            configureCell(cell as! AddElementCell)
        }
        
        return cell
    }
    
    
    
    // MARK: - Private Methods
    
    private func configureCell(_ cell: TitleSubtitleInfoCell, withProject project: Project) {
        cell.setEditMode(editMode: navigationBar.editMode, animated: false)
        cell.deleteButtonAction = deleteButtonTapped
        
        cell.titleLabel.text = project.name.uppercased()
        
        let meetingsString = project.meetings.count == 1 ?
            NSLocalizedString("PROJECTS_MEETINGS_SINGULAR", comment: "Meetings") :
            NSLocalizedString("PROJECTS_MEETINGS_PLURAL", comment: "Meetings")
        cell.subtitleLabel.text = String(format: "%d %@", project.meetings.count, meetingsString)
        
        let currency = NSLocalizedString("CURRENCY", comment: "Currency")
        cell.infoLabel.text = String(format: "%.2f%@", project.totalCost(), currency)
    }
    
    private func configureCell(_ cell: AddElementCell) {
        cell.createButtonAction = addProjectTapped
        
        let cellTitle = NSLocalizedString("PROJECTS_CREATE_PROJECT_CELL_TITLE", comment: "Create a project")
        cell.titleLabel.text = cellTitle
    }
    
}
