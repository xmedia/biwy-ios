//
//  BWCreateProfileModuleOutput.swift
//  Biwy
//
//  Created by Vlad on 11/14/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateProfileModuleOutput: class {
    
    func moduleHasFinishedTask(_ moduleInput: CreateProfileModuleInput)
    
}
