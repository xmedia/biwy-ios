//
//  MenuBWMenuPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class MenuPresenter: MenuModuleInput, MenuViewOutput {
    
    weak var view: MenuViewInput!
    weak var output: MenuModuleOutput?
    
    
    // MARK: - MenuModuleInput
    
    func configureModule(delegate: MenuModuleOutput) {
        output = delegate
    }
    
    
    
    // MARK: - MenuViewOutput
    
    func viewIsReady() {
        view.populateMenu(items: itemsArray())
    }
    
    func userSelectedMenuItem(_ item: MenuItem) {
        switch item {
        case .Home:
            output?.userSelectedHome()
        case .History:
            output?.userSelectedHistory()
        case .Projects:
            output?.userSelectedProjects()
        case .Profiles:
            output?.userSelectedProfiles()
        case .About:
            output?.userSelectedAbout()
        case .Reset:
            output?.userSelectedReset()
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func itemsArray() -> Array<MenuItem> {
        return Array<MenuItem>(arrayLiteral: MenuItem.Home,
                             MenuItem.History,
                             MenuItem.Projects,
                             MenuItem.Profiles,
                             MenuItem.About,
                             MenuItem.Reset
        )
    }
    
}
