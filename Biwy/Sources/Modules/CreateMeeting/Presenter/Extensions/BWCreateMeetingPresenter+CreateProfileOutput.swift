//
//  BWCreateMeetingPresenter+CreateProfileOutput.swift
//  Biwy
//
//  Created by Vlad on 11/15/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

extension CreateMeetingPresenter: CreateProfileModuleOutput {
    
    internal func moduleHasFinishedTask(_ moduleInput: CreateProfileModuleInput) {
        interactor.fetchProfiles()
        router.hideCreateProfileScreen(moduleInput: moduleInput)
    }
    
}
