//
//  HistoryBWHistoryViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol HistoryViewInput: class {
    
    func configureNavigation(menuControls: MenuToggleProtocol)
    func setupInitialState()
    func populateWithMeetings(_ meetings: Array<Meeting>)
    
}
