//
//  BWProfilesPresenter+CreateProfileOutput.swift
//  Biwy
//
//  Created by Vlad on 11/14/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension ProfilesPresenter: CreateProfileModuleOutput {
    
    func moduleHasFinishedTask(_ moduleInput: CreateProfileModuleInput) {
        interactor.fetchProfiles()
        router.hideCreateProfileScreen(moduleInput: moduleInput)
    }
    
}
