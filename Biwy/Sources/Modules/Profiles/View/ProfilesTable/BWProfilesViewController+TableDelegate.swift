//
//  BWProfilesViewController+TableDelegate.swift
//  Biwy
//
//  Created by Vlad on 11/12/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension ProfilesViewController: UITableViewDataSource {
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let additionalSections = navigationBar.editMode ? 0 : 1
        return 1 + additionalSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let firstSectionRows = profiles?.count ?? 0
        return section == 0 ? firstSectionRows : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            configureCell(cell as! ProfileCell, withProfile: profiles![indexPath.row])
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "AddElementCell") as! AddElementCell
            configureCell(cell as! AddElementCell)
        }
        
        return cell
    }
    
    
    
    // MARK: - Private Methods
    
    private func configureCell(_ cell: ProfileCell, withProfile profile: Profile) {
        cell.deleteButtonAction = deleteButtonTapped
        cell.profileSelectedAction = profileSelectButtonTapped
        
        cell.setEditMode(editMode: navigationBar.editMode, animated: false)
        cell.setIsMyProfile(myProfile: isMyProfile(profile), animated: false)
        
        let currency = NSLocalizedString("CURRENCY", comment: "Currency")
        cell.titleLabel.text = profile.jobTitle.uppercased()
        cell.infoLabel.text = String(format: "%.2f%@", profile.dayRate, currency)
        cell.infoDetailsLabel.text = NSLocalizedString("PROFILES_TJM", comment: "TJM")
    }
    
    private func configureCell(_ cell: AddElementCell) {
        cell.createButtonAction = addProfileTapped
        cell.titleLabel.text = NSLocalizedString("PROFILES_CREATE_PROFILE_CELL_TITLE", comment: "Create a profile")
    }
    
}
