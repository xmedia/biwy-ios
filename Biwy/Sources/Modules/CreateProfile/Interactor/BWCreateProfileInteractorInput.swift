//
//  BWCreateProfileBWCreateProfileInteractorInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol CreateProfileInteractorInput {
    
    func createProfile(jobTitle: String, dayRate: Double)
    
}
