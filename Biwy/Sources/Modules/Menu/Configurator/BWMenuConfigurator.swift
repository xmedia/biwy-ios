//
//  BWMenuBWMenuConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class MenuModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? MenuViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: MenuViewController) {
        let presenter = MenuPresenter()
        presenter.view = viewController
        
        viewController.output = presenter
    }
    
}
