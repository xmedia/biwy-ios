//
//  CreateMeetingBWCreateMeetingViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol CreateMeetingViewOutput {
    
    func viewIsReady()
    func didTapBackButton()
    func didTapSelectProject()
    func didTapAddProject()
    func didTapAddProfile()
    func didTapStartMeeting()
    
}
