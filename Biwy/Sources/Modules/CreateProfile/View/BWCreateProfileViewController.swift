//
//  CreateProfileBWCreateProfileViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import pop

class CreateProfileViewController: UIViewController, CreateProfileViewInput, UITextFieldDelegate {
    
    var output: CreateProfileViewOutput!
    
    let disposeBag = DisposeBag()
    @IBOutlet private weak var dialogTitleLabel: UILabel!
    @IBOutlet private weak var jobTitleTextField: UITextField!
    @IBOutlet private weak var dayRateView: UIView!
    @IBOutlet private weak var dayRateViewWidth: NSLayoutConstraint!
    @IBOutlet private weak var dayRateTextField: UITextField!
    @IBOutlet private weak var annualSalaryView: UIView!
    @IBOutlet private weak var annualSalaryViewWidth: NSLayoutConstraint!
    @IBOutlet private weak var annualSalaryTextField: UITextField!
    @IBOutlet private weak var dialogDescriptionLabel: UILabel!
    @IBOutlet private weak var createButton: UIButton!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - CreateProfileViewInput
    
    func setupInitialState() {
        localize()
        
        addTextFieldObservers()
        jobTitleTextField.becomeFirstResponder()
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped() {
        output.didTapCloseButton()
    }
    
    @IBAction func createButtonTapped() {
        let dayRate = Double(dayRateTextField.text ?? "0")
        let annualSalary = Double(annualSalaryTextField.text ?? "0")
        output.didTapCreateButton(jobTitle: jobTitleTextField.text!, dayRate: dayRate, annualSalary: annualSalary)
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        func localizeDialogTitle() {
            let dialogTitle = NSLocalizedString("CREATE_PROFILE_TITLE", comment: "Dialog title")
            dialogTitleLabel.text = dialogTitle
        }
        
        func localizeDialogDescription() {
            let dialogDescription = NSLocalizedString("CREATE_PROFILE_DESCRIPTION", comment: "Dialog description")
            dialogDescriptionLabel.text = dialogDescription
        }
        
        func localizeJobTitleTextFieldPlaceholder() {
            let placeholderString = NSLocalizedString("CREATE_PROFILE_TEXTFIELD_PLACEHOLDER", comment: "Job title placeholder")
            let placeholderAttributes = [NSForegroundColorAttributeName: UIColor.bwDarkGrayColor()]
            let placeholderAttributedString = NSAttributedString(string: placeholderString,
                                                                 attributes: placeholderAttributes)
            jobTitleTextField.attributedPlaceholder = placeholderAttributedString
        }
        
        func localizeDayRateTextFieldPlaceholder() {
            let placeholderString = NSLocalizedString("CREATE_PROFILE_DAY_RATE_PLACEHOLDER", comment: "Day rate placeholder")
            let placeholderAttributes = [NSForegroundColorAttributeName: UIColor.bwDarkGrayColor()]
            let placeholderAttributedString = NSAttributedString(string: placeholderString,
                                                                 attributes: placeholderAttributes)
            dayRateTextField.attributedPlaceholder = placeholderAttributedString
        }
        
        func localizeAnnualSalaryTextFieldPlaceholder() {
            let placeholderString = NSLocalizedString("CREATE_PROFILE_ANNUAL_SALARY_PLACEHOLDER", comment: "Annual salary placeholder")
            let placeholderAttributes = [NSForegroundColorAttributeName: UIColor.bwDarkGrayColor()]
            let placeholderAttributedString = NSAttributedString(string: placeholderString,
                                                                 attributes: placeholderAttributes)
            annualSalaryTextField.attributedPlaceholder = placeholderAttributedString
        }
        
        localizeDialogTitle()
        localizeDialogDescription()
        localizeJobTitleTextFieldPlaceholder()
        localizeDayRateTextFieldPlaceholder()
        localizeAnnualSalaryTextFieldPlaceholder()
    }
    
    private func addTextFieldObservers() {
        dayRateTextField.rx.text
            .map({ text -> Bool in
                let string = text ?? ""
                return string.isEmpty
            })
            .subscribe(onNext: { textIsEmpty in
                let dayRateWidth = textIsEmpty ? 108 : 286
                let annualSalaryWidth = textIsEmpty ? 163 : 0
                self.changeDayRateWidth(Double(dayRateWidth))
                self.changeAnnualSalaryWidth(Double(annualSalaryWidth))
            })
            .addDisposableTo(disposeBag)
        
        annualSalaryTextField.rx.text
            .map ({ text -> Bool in
                let string = text ?? ""
                return string.isEmpty
            }).shareReplay(1)
            .subscribe(onNext: { textIsEmpty in
                let dayRateWidth = textIsEmpty ? 108 : 0
                let annualSalaryWidth = textIsEmpty ? 163 : 286
                self.changeDayRateWidth(Double(dayRateWidth))
                self.changeAnnualSalaryWidth(Double(annualSalaryWidth))
            })
            .addDisposableTo(disposeBag)
    }
    
    private func changeDayRateWidth(_ width: Double) {
        if let dayRateWidthAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
            dayRateWidthAnimation.toValue = width
            dayRateViewWidth.pop_add(dayRateWidthAnimation, forKey: "day_rate_change_width")
        }
    }
    
    private func changeAnnualSalaryWidth(_ width: Double) {
        if let annualSalaryWidthAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
            annualSalaryWidthAnimation.toValue = width
            annualSalaryViewWidth.pop_add(annualSalaryWidthAnimation, forKey: "annual_salary_change_width")
        }
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == jobTitleTextField {
            dayRateTextField.becomeFirstResponder()
            return false
        }
        
        return true
    }
    
}
