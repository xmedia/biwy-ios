//
//  BWCreateMeetingPresenter+CreateProjectOutput.swift
//  Biwy
//
//  Created by Vlad on 11/15/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

extension CreateMeetingPresenter: CreateProjectModuleOutput {
    
    internal func moduleHasFinishedTask(_ moduleInput: CreateProjectModuleInput) {
        router.hideCreateProjectScreen(moduleInput: moduleInput)
    }
    
}
