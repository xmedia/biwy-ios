//
//  BWCreateProfileBWCreateProfileInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol CreateProfileInteractorOutput: class {
    
    func didSucceedCreateProfile()
    func didFailCreateProfile()
    
}
