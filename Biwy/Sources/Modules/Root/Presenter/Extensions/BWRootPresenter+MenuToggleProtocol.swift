//
//  BWRootPresenter+MenuToggleProtocol.swift
//  Biwy
//
//  Created by Vlad on 11/10/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension RootPresenter: MenuToggleProtocol {
    
    func activatePanGestureRecognizer() {
        view.activatePanGestureRecognizers()
    }
    
    func deactivatePanGestureRecognizer() {
        view.deactivatePanGestureRecognizers()
    }
    
    func openMenu() {
        view.openMenu()
    }
    
    func closeMenu() {
        view.closeMenu()
    }
    
}
