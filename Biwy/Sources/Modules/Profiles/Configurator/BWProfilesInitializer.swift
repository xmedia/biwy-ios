//
//  BWProfilesBWProfilesInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProfilesModuleInitializer: NSObject {
    
    @IBOutlet weak var profilesViewController: ProfilesViewController!
    
    override func awakeFromNib() {
        let configurator = ProfilesModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: profilesViewController)
    }
    
}
