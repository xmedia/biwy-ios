//
//  RootBWRootPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class RootPresenter: RootViewOutput {
	
    weak var view: RootViewInput!
    var router: RootRouterInput!
    
    private weak var sideMenu: MenuModuleInput?
    internal weak var currentScreen: UIViewController?


    // MARK: - RootViewOutput

    func viewIsReady() {
        addSideMenu()
        showHomeScreen()
    }
    
    
    
    // MARK: - Private Methods
    
    internal func addSideMenu() {
        let viewController = view as! UIViewController
        sideMenu = router.showSideMenu(onParentController: viewController, containerView: view.menuContainer)
        sideMenu!.configureModule(delegate: self)
    }
    
    internal func showHomeScreen() {
        let viewController = view as! UIViewController
        let homeModule = router.showHomeScreen(onParentController: viewController, containerView: view.screenContainer)
        homeModule.configureModule(menuControls: self)
        currentScreen = (homeModule as! HomePresenter).view as! HomeViewController
    }
    
    internal func showHistoryScreen() {
        let viewController = view as! UIViewController
        let historyModule = router.showHistoryScreen(onParentController: viewController, containerView: view.screenContainer)
        historyModule.configureModule(menuControls: self)
        currentScreen = (historyModule as! HistoryPresenter).view as! HistoryViewController
    }
    
    internal func showProjectsScreen() {
        let viewController = view as! UIViewController
        let projectsModule = router.showProjectsScreen(onParentController: viewController, containerView: view.screenContainer)
        projectsModule.configureModule(menuControls: self)
        currentScreen = (projectsModule as! ProjectsPresenter).view as! ProjectsViewController
    }
    
    internal func showProfilesScreen() {
        let viewController = view as! UIViewController
        let profilesModule = router.showProfilesScreen(onParentController: viewController, containerView: view.screenContainer)
        profilesModule.configureModule(menuControls: self)
        currentScreen = (profilesModule as! ProfilesPresenter).view as! ProfilesViewController
    }
    
    internal func showAboutScreen() {
        let viewController = view as! UIViewController
        let aboutModule = router.showAboutScreen(onParentController: viewController, containerView: view.screenContainer)
        currentScreen = (aboutModule as! AboutPresenter).view as! AboutViewController
    }
    
}
