//
//  RootBWRootRouter.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class RootRouter: RootRouterInput {
    
    func showSideMenu(onParentController parentController: UIViewController, containerView: UIView) -> MenuModuleInput {
        let sideMenu = UIStoryboard(name: "Menu", bundle: nil).instantiateInitialViewController() as! MenuViewController
        
        sideMenu.willMove(toParentViewController: parentController)
        sideMenu.view.frame = containerView.bounds
        containerView.addSubview(sideMenu.view)
        parentController.addChildViewController(sideMenu)
        sideMenu.didMove(toParentViewController: parentController)
        
        return sideMenu.output as! MenuModuleInput
    }
    
    func showHomeScreen(onParentController parentController: UIViewController, containerView: UIView) -> HomeModuleInput {
        let navigationController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let homeScreen = navigationController.topViewController as! HomeViewController
        
        navigationController.willMove(toParentViewController: parentController)
        navigationController.view.frame = containerView.bounds
        containerView.addSubview(navigationController.view)
        parentController.addChildViewController(navigationController)
        parentController.didMove(toParentViewController: parentController)
        
        return homeScreen.output as! HomeModuleInput
    }
    
    func showHistoryScreen(onParentController parentController: UIViewController, containerView: UIView) -> HistoryModuleInput {
        let historyScreen = UIStoryboard(name: "History", bundle: nil).instantiateInitialViewController() as! HistoryViewController
        
        historyScreen.willMove(toParentViewController: parentController)
        historyScreen.view.frame = containerView.bounds
        containerView.addSubview(historyScreen.view)
        parentController.addChildViewController(historyScreen)
        historyScreen.didMove(toParentViewController: parentController)
        
        return historyScreen.output as! HistoryModuleInput
    }
    
    func showProjectsScreen(onParentController parentController: UIViewController, containerView: UIView) -> ProjectsModuleInput {
        let projectsScreen = UIStoryboard(name: "Projects", bundle: nil).instantiateInitialViewController() as! ProjectsViewController
        
        projectsScreen.willMove(toParentViewController: parentController)
        projectsScreen.view.frame = containerView.bounds
        containerView.addSubview(projectsScreen.view)
        parentController.addChildViewController(projectsScreen)
        projectsScreen.didMove(toParentViewController: parentController)
        
        return projectsScreen.output as! ProjectsModuleInput
    }
    
    func showProfilesScreen(onParentController parentController: UIViewController, containerView: UIView) -> ProfilesModuleInput {
        let profilesScreen = UIStoryboard(name: "Profiles", bundle: nil).instantiateInitialViewController() as! ProfilesViewController
        
        profilesScreen.willMove(toParentViewController: parentController)
        profilesScreen.view.frame = containerView.bounds
        containerView.addSubview(profilesScreen.view)
        parentController.addChildViewController(profilesScreen)
        profilesScreen.didMove(toParentViewController: parentController)
        
        return profilesScreen.output as! ProfilesModuleInput
    }
    
    func showAboutScreen(onParentController parentController: UIViewController, containerView: UIView) -> AboutModuleInput {
        let aboutScreen = UIStoryboard(name: "About", bundle: nil).instantiateInitialViewController() as! AboutViewController
        
        aboutScreen.willMove(toParentViewController: parentController)
        aboutScreen.view.frame = containerView.bounds
        containerView.addSubview(aboutScreen.view)
        parentController.addChildViewController(aboutScreen)
        aboutScreen.didMove(toParentViewController: parentController)
        
        return aboutScreen.output as! AboutModuleInput
    }
    
    func hideScreen(screenController: UIViewController) {
        screenController.willMove(toParentViewController: nil)
        screenController.removeFromParentViewController()
        screenController.view.removeFromSuperview()
    }
    
}
