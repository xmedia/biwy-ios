//
//  ProjectsBWProjectsRouter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProjectsRouter: ProjectsRouterInput {
    
    func showCreateProjectScreen(onParentController parentController: UIViewController) -> CreateProjectModuleInput {
        let createProjectScreen = UIStoryboard(name: "CreateProject", bundle: nil).instantiateInitialViewController() as! CreateProjectViewController
        
        createProjectScreen.modalPresentationStyle = .fullScreen
        parentController.present(createProjectScreen, animated: false)
        
        return createProjectScreen.output as! CreateProjectModuleInput
    }
    
    func hideCreateProjectScreen(moduleInput: CreateProjectModuleInput) {
        let presenter = moduleInput as! CreateProjectPresenter
        let controller = presenter.view as! CreateProjectViewController
        
        controller.modalTransitionStyle = .coverVertical
        controller.dismiss(animated: false)
    }
    
}
