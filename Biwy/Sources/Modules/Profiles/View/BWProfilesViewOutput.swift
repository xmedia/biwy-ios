//
//  ProfilesBWProfilesViewOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol ProfilesViewOutput {
    
    func viewIsReady()
    func userDidTapToRemoveProfile(_ profile: Profile)
    func userDidTapToChangeMyProfile(_ myProfile: MyProfile, toProfile profile: Profile)
    func didTapAddProfile()
    
}
