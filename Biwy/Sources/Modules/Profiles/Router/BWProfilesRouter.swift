//
//  ProfilesBWProfilesRouter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class ProfilesRouter: ProfilesRouterInput {
    
    func showCreateProfileScreen(onParentController parentController: UIViewController) -> CreateProfileModuleInput {
        let createProfileScreen = UIStoryboard(name: "CreateProfile", bundle: nil).instantiateInitialViewController() as! CreateProfileViewController
        
        createProfileScreen.modalPresentationStyle = .fullScreen
        parentController.present(createProfileScreen, animated: false)
        
        return createProfileScreen.output as! CreateProfileModuleInput
    }
    
    func hideCreateProfileScreen(moduleInput: CreateProfileModuleInput) {
        let presenter = moduleInput as! CreateProfilePresenter
        let controller = presenter.view as! CreateProfileViewController
        
        controller.modalTransitionStyle = .coverVertical
        controller.dismiss(animated: false)
    }
    
}
