//
//  ProjectsBWProjectsPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class ProjectsPresenter: ProjectsModuleInput, ProjectsViewOutput, ProjectsInteractorOutput {
    
    weak var view: ProjectsViewInput!
    var interactor: ProjectsInteractorInput!
    var router: ProjectsRouterInput!
    
    
    // MARK: - ProjectsModuleInput
    
    func configureModule(menuControls: MenuToggleProtocol) {
        view.configureNavigation(menuControls: menuControls)
    }
    
    
    
    // MARK: - ProjectsViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
        interactor.fetchProjects()
    }
    
    func userDidTapToRemoveProject(_ project: Project) {
        interactor.removeProject(project)
    }
    
    func didTapAddProject() {
        showCreateProjectScreen()
    }
    
    
    
    // MARK: - ProjectsInteractorOutput
    
    func didSucceedProjectsFetch(projects: Array<Project>) {
        view.populateWithProjects(projects)
    }
    
    func didFailProjectsFetch() {
        // do nothing, show nothing
    }
    
    func didSucceedProjectRemoval() {
        // do nothing, it was removed
    }
    
    func didFailProjectRemoval() {
        // TODO: aici tre de facut ceva
    }
    
    
    
    // MARK: - Private Methods
    
    internal func showCreateProjectScreen() {
        let viewController = view as! UIViewController
        let createProjectModule = router.showCreateProjectScreen(onParentController: viewController)
        createProjectModule.configureModule(delegate: self)
    }
    
}
