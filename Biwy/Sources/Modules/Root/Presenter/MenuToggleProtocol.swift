//
//  MenuToggleProtocol.swift
//  Biwy
//
//  Created by Vlad on 11/10/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol MenuToggleProtocol {
    
    func activatePanGestureRecognizer()
    func deactivatePanGestureRecognizer()
    
    func openMenu()
    func closeMenu()
    
}
