//
//  CreateProjectBWCreateProjectViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreateProjectViewController: UIViewController, CreateProjectViewInput, UITextFieldDelegate {
    
    var output: CreateProjectViewOutput!
    
    let disposeBag = DisposeBag()
    @IBOutlet private weak var dialogTitleLabel: UILabel!
    @IBOutlet private weak var projectTitleTextField: UITextField!
    @IBOutlet private weak var dialogDescriptionLabel: UILabel!
    @IBOutlet private weak var createButton: UIButton!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - CreateProjectViewInput
    
    func setupInitialState() {
        localize()
        
        addProjectTitleTextFieldObservers()
        projectTitleTextField.becomeFirstResponder()
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func closeButtonTapped() {
        output.didTapCloseButton()
    }
    
    @IBAction func createButtonTapped() {
        output.didTapCreateButton(projectName: projectTitleTextField.text!)
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        func localizeDialogTitle() {
            let dialogTitle = NSLocalizedString("CREATE_PROJECT_TITLE", comment: "Dialog title")
            dialogTitleLabel.text = dialogTitle
        }
        
        func localizeDialogDescription() {
            let dialogDescription = NSLocalizedString("CREATE_PROJECT_DESCRIPTION", comment: "Dialog description")
            dialogDescriptionLabel.text = dialogDescription
        }
        
        func localizeProjectTitleTextFieldPlaceholder() {
            let placeholderString = NSLocalizedString("CREATE_PROJECT_TEXTFIELD_PLACEHOLDER", comment: "Textfield placeholder")
            let placeholderAttributes = [NSForegroundColorAttributeName: UIColor.bwDarkGrayColor()]
            let placeholderAttributedString = NSAttributedString(string: placeholderString,
                                                                 attributes: placeholderAttributes)
            projectTitleTextField.attributedPlaceholder = placeholderAttributedString
        }
        
        localizeDialogTitle()
        localizeDialogDescription()
        localizeProjectTitleTextFieldPlaceholder()
    }
    
    private func addProjectTitleTextFieldObservers() {
        projectTitleTextField.rx.text
            .map({
                let text = ($0 ?? "")
                return !text.isEmpty && self.output.canCreateProjectWithName(text)
            })
            .shareReplay(1)
            .bindTo(createButton.rx.isEnabled)
            .addDisposableTo(disposeBag)
        
        projectTitleTextField.rx
            .controlEvent(.editingDidEndOnExit)
            .subscribe(onNext: { [weak self] in
                self?.createButtonTapped()
            })
            .addDisposableTo(disposeBag)
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text, !text.isEmpty else {
            return false
        }
        
        return self.output.canCreateProjectWithName(text)
    }
    
}
