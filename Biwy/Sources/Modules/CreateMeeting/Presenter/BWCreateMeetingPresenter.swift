//
//  CreateMeetingBWCreateMeetingPresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateMeetingPresenter: CreateMeetingModuleInput, CreateMeetingViewOutput, CreateMeetingInteractorOutput {
	
    weak var view: CreateMeetingViewInput!
    var interactor: CreateMeetingInteractorInput!
    var router: CreateMeetingRouterInput!
    
    
    // MARK: - CreateMeetingViewOutput

    func viewIsReady() {
        view.setupInitialState()
        interactor.fetchProfiles()
    }
    
    func didTapBackButton() {
        let viewController = view as! UIViewController
        router.goBack(viewController: viewController)
    }
    
    func didTapSelectProject() {
        
    }
    
    func didTapAddProject() {
        showCreateProjectScreen()
    }
    
    func didTapAddProfile() {
        showCreateProfileScreen()
    }
    
    func didTapStartMeeting() {
        
    }
    
    
    
    // MARK: - CreateMeetingInteractorOutput
    
    func didSucceedFetchProfiles(_ profiles: Array<Profile>) {
        view.populateWithProfiles(profiles)
    }
    
    func didFailFetchProfiles() {
        // do nothing, show nothing
    }
    
    
    
    // MARK: - Private Methods
    
    private func showCreateProjectScreen() {
        let viewController = view as! UIViewController
        let createProjectModule = router.showCreateProjectScreen(onParentController: viewController)
        createProjectModule.configureModule(delegate: self)
    }
    
    private func showCreateProfileScreen() {
        let viewController = view as! UIViewController
        let createProfileModule = router.showCreateProfileScreen(onParentController: viewController)
        createProfileModule.configureModule(delegate: self)
    }
    
}
