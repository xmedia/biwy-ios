//
//  BWCreateMeetingBWCreateMeetingInteractorInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol CreateMeetingInteractorInput {
    
    func fetchProfiles()
    
}
