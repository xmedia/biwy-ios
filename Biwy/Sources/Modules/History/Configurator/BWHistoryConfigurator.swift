//
//  BWHistoryBWHistoryConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class HistoryModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? HistoryViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: HistoryViewController) {
        let router = HistoryRouter()
        
        let presenter = HistoryPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = HistoryInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
