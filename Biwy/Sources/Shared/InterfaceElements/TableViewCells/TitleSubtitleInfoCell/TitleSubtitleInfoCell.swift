//
//  TitleSubtitleInfoCell.swift
//  Biwy
//
//  Created by Vlad on 11/10/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import pop

class TitleSubtitleInfoCell: UITableViewCell {
    
    @IBOutlet private weak var deleteButtonLeading: NSLayoutConstraint!
    private var editMode: Bool = false
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    var deleteButtonAction: ((TitleSubtitleInfoCell) -> ())?
    
    
    // MARK: - Public Methods
    
    func setEditMode(editMode: Bool, animated: Bool) {
        self.editMode = editMode
        
        if animated {
            if let deleteButtonAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
                deleteButtonAnimation.toValue = editMode ? -6 : -46
                deleteButtonAnimation.springBounciness = 0
                deleteButtonLeading.pop_add(deleteButtonAnimation, forKey: "toggle_delete_button")
            }
        } else {
            deleteButtonLeading.constant = editMode ? -6 : -46
        }
    }
    
    
    
    // MARK: - Action
    
    @IBAction private func deleteButtonTapped() {
        deleteButtonAction?(self)
    }
    
}
