//
//  ProfileCell.swift
//  Biwy
//
//  Created by Vlad on 11/12/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import pop

class ProfileCell: UITableViewCell {
    
    @IBOutlet private weak var deleteButtonLeading: NSLayoutConstraint!
    
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var subtitleLabelLeading: NSLayoutConstraint!
    
    @IBOutlet private weak var profileCheckmarkImage: UIImageView!
    @IBOutlet private weak var profileCheckmarkButton: UIButton!
    @IBOutlet private weak var profileCheckmarkImageWidth: NSLayoutConstraint!
    
    private var editMode: Bool = false
    private var isMyProfile: Bool = false
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoDetailsLabel: UILabel!
    
    var deleteButtonAction: ((ProfileCell) -> ())?
    var profileSelectedAction: ((ProfileCell) -> ())?
    
    
    
    // MARK: - View Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localize()
    }
    
    
    
    // MARK: - Public Methods
    
    func setIsMyProfile(myProfile: Bool, animated: Bool) {
        isMyProfile = myProfile
        profileCheckmarkImage.image = isMyProfile ? UIImage(named: "profiles_my_profile_full") : UIImage(named: "profiles_my_profile_empty")
        
        if animated {
            if let subtitleLabelAlphaAnimation = POPBasicAnimation(propertyNamed: kPOPViewAlpha) {
                subtitleLabelAlphaAnimation.toValue = myProfile ? 1 : 0
                subtitleLabel.pop_add(subtitleLabelAlphaAnimation, forKey: "toggle_subtitle_label")
            }
        } else {
            subtitleLabel.alpha = myProfile ? 1 : 0
        }
    }
    
    func setEditMode(editMode: Bool, animated: Bool) {
        self.editMode = editMode
        
        profileCheckmarkButton.isHidden = !editMode
        if animated {
            if let deleteButtonAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
                deleteButtonAnimation.toValue = editMode ? -6 : -46
                deleteButtonAnimation.springBounciness = 0
                deleteButtonLeading.pop_add(deleteButtonAnimation, forKey: "toggle_delete_button")
            }
            
            if let profileCheckmarkWidthAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
                profileCheckmarkWidthAnimation.toValue = editMode ? 14 : 0
                profileCheckmarkWidthAnimation.duration = 0.3
                profileCheckmarkImageWidth.pop_add(profileCheckmarkWidthAnimation, forKey: "toggle_profile_button")
            }
            
            if let subtitleLabelAnimation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
                subtitleLabelAnimation.toValue = editMode ? 6 : 0
                subtitleLabelLeading.pop_add(subtitleLabelAnimation, forKey: "toggle_subtitle_leading")
            }
        } else {
            deleteButtonLeading.constant = editMode ? -6 : -46
            profileCheckmarkImageWidth.constant = editMode ? 14 : 0
            subtitleLabelLeading.constant = editMode ? 6 : 0
        }
    }
    
    
    
    // MARK: - Action
    
    @IBAction private func deleteButtonTapped() {
        deleteButtonAction?(self)
    }
    
    @IBAction private func myProfileButtonTapped() {
        profileSelectedAction?(self)
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        subtitleLabel.text = NSLocalizedString("PROFILES_MY_PROFILE", comment: "My profile")
    }
    
}
