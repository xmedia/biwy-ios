//
//  BWCreateProjectBWCreateProjectInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateProjectInteractor: CreateProjectInteractorInput {
    
    weak var output: CreateProjectInteractorOutput!
    private let realm = SharedRealm.realm
    
    // MARK: - CreateProjectInteractorInput
    
    func isProjectNameUnique(_ projectName: String) -> Bool {
        guard let realm = realm else {
            return false
        }
        
        return realm.objects(Project.self).filter("name ==[c] '\(projectName)'").count == 0
    }
    
    func createProject(name: String) {
        guard let realm = realm else {
            output.didFailCreateProject()
            return
        }
        
        let project = Project(value: [name, []])
        do {
            try realm.write {
                realm.add(project)
            }
            output.didSucceedCreateProject()
        } catch let error as NSError {
            print(error)
            output.didFailCreateProject()
        }
    }
    
}
