//
//  HomeBWHomeViewInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

protocol HomeViewInput: class {
    
    func setupInitialState()
    func configureNavigation(menuControls: MenuToggleProtocol)
    func showReport(totalTime: String, meetings: Array<Meeting>, projects: Int, individualCost: String)
    
}
