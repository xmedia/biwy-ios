//
//  BWHomeBWHomeInteractorOutput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol HomeInteractorOutput: class {
    
    func didSucceedCurrentReportFetch(totalTime: Double, meetings: Array<Meeting>, projects: Int, individualCost: Double)
    func didFailCurrentReportFetch()
    
}
