//
//  AddProjectCell.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class AddElementCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var createButtonAction: (() -> ())?
    
    
    // MARK: - Action
    
    @IBAction private func createButtonTapped() {
        createButtonAction?()
    }
    
}
