//
//  HomeBWHomeRouterInput.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol HomeRouterInput {
    
    func showHistoryScreen(onParentController parentController: UIViewController, containerView: UIView)
    func showCreateMeetingScreen(sourceController: UIViewController)
    
}
