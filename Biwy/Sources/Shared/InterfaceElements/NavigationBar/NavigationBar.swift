//
//  NavigationBar.swift
//  Biwy
//
//  Created by Vlad on 11/10/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import pop

class NavigationBar: UIView {
    
    // MARK: - Private Variables
    
    @IBOutlet private weak var view: UIView!
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var editButton: UIButton!
    @IBOutlet private weak var doneButton: UIButton!
    
    @IBOutlet private weak var editButtonTrailing: NSLayoutConstraint!
    @IBOutlet private weak var doneButtonTrailing: NSLayoutConstraint!
    
    public private(set) var editMode: Bool = false {
        didSet {
            animateEditModeToggle()
        }
    }
    
    
    
    // MARK: - Public Variabless
    
    var menuControls: MenuToggleProtocol?
    
    var editModeStateChanged: (() -> ())?
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var showOnlyMenuButton: Bool = false {
        didSet {
            titleLabel.isHidden = showOnlyMenuButton
            editButton.isHidden = showOnlyMenuButton
            doneButton.isHidden = showOnlyMenuButton
        }
    }
    
    
    
    // MARK: -
    // MARK: - View Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        localize()
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func menuButtonTapped() {
        menuControls?.openMenu()
    }
    
    @IBAction func editButtonTapped() {
        editMode = true
    }
    
    @IBAction func doneButtonTapped() {
        editMode = false
    }
    
    
    
    // MARK: - Private Methods
    
    private func nibSetup() {
        _ = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as! UIView
        view.frame = bounds
        addSubview(view)
    }
    
    private func localize() {
        let doneButtonTitle = NSLocalizedString("NAVIGATION_DONE_BUTTON", comment: "Done button")
        doneButton.setTitle(doneButtonTitle, for: .normal)
    }
    
    private func animateEditModeToggle() {
        if let editButtonAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
            editButtonAnimation.toValue = editMode ? -50 : 14
            editButtonAnimation.springBounciness = 0
            editButtonTrailing.pop_add(editButtonAnimation, forKey: "toggle_edit_button")
        }
        
        if let doneButtonAnimation = POPSpringAnimation(propertyNamed: kPOPLayoutConstraintConstant) {
            doneButtonAnimation.toValue = editMode ? 14 : -50
            doneButtonAnimation.springBounciness = 0
            doneButtonTrailing.pop_add(doneButtonAnimation, forKey: "toggle_done_button")
        }
        
        editModeStateChanged?()
    }
    
}
