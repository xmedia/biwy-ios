//
//  ProjectsBWProjectsViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProjectsViewController: UIViewController, ProjectsViewInput {
    
    var output: ProjectsViewOutput!
    
    @IBOutlet internal weak var navigationBar: NavigationBar!
    @IBOutlet internal weak var projectsTable: UITableView!
    internal var projects: Array<Project>?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - ProjectsViewInput
    
    func configureNavigation(menuControls: MenuToggleProtocol) {
        navigationBar.menuControls = menuControls
        navigationBar.editModeStateChanged = editModeStateChanged
    }
    
    func setupInitialState() {
        localize()
        registerTableCustomCells()
    }
    
    func populateWithProjects(_ projects: Array<Project>) {
        self.projects = projects
        projectsTable.reloadData()
    }
    
    
    
    // MARK: - Actions
    
    func addProjectTapped() {
        output.didTapAddProject()
    }
    
    func editModeStateChanged() {
        projectsTable.visibleCells.forEach { cell in
            if let titleSubtitleInfoCell = cell as? TitleSubtitleInfoCell {
                titleSubtitleInfoCell.setEditMode(editMode: navigationBar.editMode, animated: true)
            }
        }
        
        if navigationBar.editMode {
            projectsTable.deleteSections([1], with: .top)
        } else {
            projectsTable.insertSections([1], with: .top)
        }
    }
    
    func deleteButtonTapped(cell: TitleSubtitleInfoCell) {
        if let indexPathOfTappedCell =  projectsTable.indexPath(for: cell) {
            output.userDidTapToRemoveProject(projects![indexPathOfTappedCell.row])
            projects!.remove(at: indexPathOfTappedCell.row)
            projectsTable.deleteRows(at: [indexPathOfTappedCell], with: .left)
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        let screenTitle = NSLocalizedString("PROJECTS_SCREEN_TITLE", comment: "Screen title")
        navigationBar.title = screenTitle
    }
    
    private func registerTableCustomCells() {
        let titleSubtitleInfoCell = UINib(nibName: "TitleSubtitleInfoCell", bundle: nil)
        projectsTable.register(titleSubtitleInfoCell, forCellReuseIdentifier: "TitleSubtitleInfoCell")
        
        let addElementCell = UINib(nibName: "AddElementCell", bundle: nil)
        projectsTable.register(addElementCell, forCellReuseIdentifier: "AddElementCell")
    }
    
}
