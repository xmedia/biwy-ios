//
//  BWHomeBWHomeInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 05/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class HomeInteractor: HomeInteractorInput {
    
    weak var output: HomeInteractorOutput!
    private let realm = SharedRealm.realm
    
    
    // MARK: - HomeInteractorInput
    
    func fetchCurrentReport() {
        guard let realm = realm else {
            output.didFailCurrentReportFetch()
            return
        }
        
        let sortedMeetings = Array(realm.objects(Meeting.self).sorted(byProperty: "date", ascending: false))
        let projectsAmount = realm.objects(Project.self).count
        let myProfile = realm.objects(MyProfile.self).first!
        
        let totalTime = durationForMeetings(sortedMeetings)
        let individualCost = profileCost(myProfile.profile!, time: totalTime)
        
        output.didSucceedCurrentReportFetch(totalTime: totalTime,
                                            meetings: Array(sortedMeetings),
                                            projects: projectsAmount,
                                            individualCost: individualCost)
    }
    
    
    
    // MARK: - Private Methods
    
    private func profileCost(_ profile: Profile, time: Double) -> Double {
        return profile.pricePerTime(inSeconds: time)
    }
    
    private func durationForMeetings(_ meetings: Array<Meeting>) -> Double {
        return meetings.reduce(0) { $0 + $1.duration }
    }
    
}
