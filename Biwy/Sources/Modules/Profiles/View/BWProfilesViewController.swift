//
//  ProfilesBWProfilesViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProfilesViewController: UIViewController, ProfilesViewInput {
    
    var output: ProfilesViewOutput!
    
    @IBOutlet internal weak var navigationBar: NavigationBar!
    @IBOutlet internal weak var profilesTable: UITableView!
    internal var profiles: Array<Profile>?
    internal var myProfile: MyProfile?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - ProfilesViewInput
    
    func configureNavigation(menuControls: MenuToggleProtocol) {
        navigationBar.menuControls = menuControls
        navigationBar.editModeStateChanged = editModeStateChanged
    }
    
    func setupInitialState() {
        localize()
        registerTableCustomCells()
    }
    
    func populateWithProfiles(_ profiles: Array<Profile>, myProfile: MyProfile) {
        self.profiles = profiles
        self.myProfile = myProfile
        profilesTable.reloadData()
    }
    
    func updateMyProfile(_ myProfile: MyProfile) {
        self.myProfile = myProfile
        
        let profileToBeSetAsMyProfile = profiles!.filter { return isMyProfile($0) }.first!
        let profileIndex = profiles!.index(of: profileToBeSetAsMyProfile)
        if let profileCell = profilesTable.cellForRow(at: IndexPath(row: profileIndex!, section: 0)) as? ProfileCell {
            profileCell.setIsMyProfile(myProfile: true, animated: true)
        }
    }
    
    
    
    // MARK: - Actions
    
    func addProfileTapped() {
        output.didTapAddProfile()
    }
    
    func editModeStateChanged() {
        profilesTable.visibleCells.forEach { cell in
            if let profileCell = cell as? ProfileCell {
                profileCell.setEditMode(editMode: navigationBar.editMode, animated: true)
            }
        }
        
        if navigationBar.editMode {
            profilesTable.deleteSections([1], with: .top)
        } else {
            profilesTable.insertSections([1], with: .top)
        }
    }
    
    func deleteButtonTapped(cell: ProfileCell) {
        if let indexPathOfTappedCell =  profilesTable.indexPath(for: cell) {
            output.userDidTapToRemoveProfile(profiles![indexPathOfTappedCell.row])
            profiles!.remove(at: indexPathOfTappedCell.row)
            profilesTable.deleteRows(at: [indexPathOfTappedCell], with: .left)
        }
    }
    
    func profileSelectButtonTapped(cell: ProfileCell) {
        let profileSetAsMyProfile = profiles!.filter { return isMyProfile($0) }.first!
        let profileIndex = profiles!.index(of: profileSetAsMyProfile)
        if let profileCell = profilesTable.cellForRow(at: IndexPath(row: profileIndex!, section: 0)) as? ProfileCell {
            profileCell.setIsMyProfile(myProfile: false, animated: true)
        }
        
        if let myProfile = myProfile {
            if let indexPathOfTappedCell = profilesTable.indexPath(for: cell) {
                let selectedProfile = profiles![indexPathOfTappedCell.row]
                output.userDidTapToChangeMyProfile(myProfile, toProfile: selectedProfile)
            }
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        let screenTitle = NSLocalizedString("PROFILES_SCREEN_TITLE", comment: "Screen title")
        navigationBar.title = screenTitle
    }
    
    private func registerTableCustomCells() {
        let profileCell = UINib(nibName: "ProfileCell", bundle: nil)
        profilesTable.register(profileCell, forCellReuseIdentifier: "ProfileCell")
        
        let addElementCell = UINib(nibName: "AddElementCell", bundle: nil)
        profilesTable.register(addElementCell, forCellReuseIdentifier: "AddElementCell")
    }
    
    internal func isMyProfile(_ profile: Profile) -> Bool {
        if let myProfile = myProfile {
            return profile == myProfile.profile
        }
        
        return false
    }
    
}
