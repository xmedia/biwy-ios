//
//  Meeting.swift
//  Biwy
//
//  Created by Vlad on 11/3/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation
import RealmSwift

class Meeting: Object {
    
    //-- Data
    dynamic var id = NSUUID().uuidString
    dynamic var date = NSDate()
    dynamic var duration: Double = 0
    
    //-- Relationships
    let participants = List<Profile>()
    
    private let projects = LinkingObjects(fromType: Project.self, property: "meetings")
    var project: Project? {
        return projects.first
    }
    
    
    
    // MARK: - Realm Methods
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
    // MARK: - Public Methods
    
    func totalCost() -> Double {
        return participants.reduce(0.0) {
            $0 + $1.pricePerTime(inSeconds: duration)
        }
    }

}
