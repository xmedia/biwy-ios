//
//  BWProfilesBWProfilesInteractor.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class ProfilesInteractor: ProfilesInteractorInput {
    
    weak var output: ProfilesInteractorOutput!
    private let realm = SharedRealm.realm
    
    
    // MARK: - ProfilesInteractorInput
    
    func fetchProfiles() {
        guard let realm = realm else {
            output.didFailProfilesFetch()
            return
        }
        
        let profiles = realm.objects(Profile.self).sorted(byProperty: "jobTitle")
        let myProfile = realm.objects(MyProfile.self).first!
        output.didSucceedProfilesFetch(profiles: Array(profiles), myProfile: myProfile)
    }
    
    func setMyProfile(_ myProfile: MyProfile, toProfile profile: Profile) {
        guard let realm = realm else {
            output.didFailSetMyProfile()
            return
        }
        
        do {
            try realm.write {
                myProfile.profile = profile
            }
            output.didSucceedSetMyProfile(myProfile)
        } catch let error as NSError {
            print(error)
            output.didFailSetMyProfile()
        }
    }
    
    
    func removeProfile(_ profile: Profile) {
        guard let realm = realm else {
            output.didFailProfileRemoval()
            return
        }
        
        do {
            try realm.write {
                realm.delete(profile)
            }
            output.didSucceedProfileRemoval()
        } catch let error as NSError {
            print(error)
            output.didFailProfileRemoval()
        }
    }
    
}
