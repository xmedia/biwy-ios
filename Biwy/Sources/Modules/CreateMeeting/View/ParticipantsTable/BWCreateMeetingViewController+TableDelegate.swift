//
//  BWCreateMeetingViewController+TableDelegate.swift
//  Biwy
//
//  Created by Vlad on 11/15/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

extension CreateMeetingViewController: UITableViewDataSource {

    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let firstSectionRows = participants?.count ?? 0
        return section == 0 ? firstSectionRows : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ParticipantCell") as! ParticipantCell
            configureCell(cell as! ParticipantCell, withProfile: participants![indexPath.row])
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "AddElementCell") as! AddElementCell
            configureCell(cell as! AddElementCell)
        }
        
        return cell
    }
    
    
    
    // MARK: - Private Methods
    
    private func configureCell(_ cell: ParticipantCell, withProfile profile: Profile) {
        cell.participantsAmountHasChangedAction = participantsAmountHasChanged
        
        cell.jobTitleLabel.text = profile.jobTitle
        
        let dayRateString = NSLocalizedString("CREATE_MEETING_PROFILE_DAY_RATE", comment: "Day rate")
        let currency = NSLocalizedString("CURRENCY", comment: "Currency")
        cell.dayRateLabel.text = String(format: "%@ : %.0f%@", dayRateString, profile.dayRate, currency)
    }
    
    private func configureCell(_ cell: AddElementCell) {
        cell.createButtonAction = addProfileTapped
        cell.titleLabel.text = NSLocalizedString("CREATE_MEETING_CREATE_PROFILE", comment: "Create a profile")
    }
    
}
