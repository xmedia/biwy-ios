//
//  BWProjectsBWProjectsConfigurator.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProjectsModuleConfigurator {
    
    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {
        if let viewController = viewInput as? ProjectsViewController {
            configure(viewController: viewController)
        }
    }
    
    private func configure(viewController: ProjectsViewController) {
        let router = ProjectsRouter()
        
        let presenter = ProjectsPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = ProjectsInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
