//
//  HistoryBWHistoryViewController.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, HistoryViewInput {
    
    var output: HistoryViewOutput!
    
    @IBOutlet internal weak var navigationBar: NavigationBar!
    @IBOutlet private weak var meetingsTable: UITableView!
    internal var meetings: Array<Meeting>?
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    
    
    
    // MARK: - HistoryViewInput
    
    func configureNavigation(menuControls: MenuToggleProtocol) {
        navigationBar.menuControls = menuControls
        navigationBar.editModeStateChanged = editModeStateChanged
    }
    
    func setupInitialState() {
        localize()
        registerTableCustomCell()
    }
    
    func populateWithMeetings(_ meetings: Array<Meeting>) {
        self.meetings = meetings
        meetingsTable.reloadData()
    }
    
    
    
    // MARK: - Actions
    
    func editModeStateChanged() {
        meetingsTable.visibleCells.forEach { cell in
            if let titleSubtitleInfoCell = cell as? TitleSubtitleInfoCell {
                titleSubtitleInfoCell.setEditMode(editMode: navigationBar.editMode, animated: true)
            }
        }
    }
    
    func deleteButtonTapped(cell: TitleSubtitleInfoCell) {
        if let indexPathOfTappedCell =  meetingsTable.indexPath(for: cell) {
            output.userDidTapToRemoveMeeting(meetings![indexPathOfTappedCell.row])
            meetings!.remove(at: indexPathOfTappedCell.row)
            meetingsTable.deleteRows(at: [indexPathOfTappedCell], with: .left)
        }
    }
    
    
    
    // MARK: - Private Methods
    
    private func localize() {
        let screenTitle = NSLocalizedString("HISTORY_SCREEN_TITLE", comment: "Screen title")
        navigationBar.title = screenTitle
    }
    
    private func registerTableCustomCell() {
        let nib = UINib(nibName: "TitleSubtitleInfoCell", bundle: nil)
        meetingsTable.register(nib, forCellReuseIdentifier: "TitleSubtitleInfoCell")
    }
    
}
