//
//  BWMenuModuleOutput.swift
//  Biwy
//
//  Created by Vlad on 11/9/16.
//  Copyright © 2016 Biwy. All rights reserved.
//

import Foundation

protocol MenuModuleOutput: class {
    
    func userSelectedHome()
    func userSelectedHistory()
    func userSelectedProjects()
    func userSelectedProfiles()
    func userSelectedAbout()
    func userSelectedReset()
    
}
