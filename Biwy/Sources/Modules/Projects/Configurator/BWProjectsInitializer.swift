//
//  BWProjectsBWProjectsInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 09/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class ProjectsModuleInitializer: NSObject {
    
    @IBOutlet weak var projectsViewController: ProjectsViewController!
    
    override func awakeFromNib() {
        let configurator = ProjectsModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: projectsViewController)
    }
    
}
