//
//  CreateProfileBWCreateProfilePresenter.swift
//  Biwy
//
//  Created by Vlad Ionita on 14/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

class CreateProfilePresenter: CreateProfileModuleInput, CreateProfileViewOutput, CreateProfileInteractorOutput {
	
    weak var view: CreateProfileViewInput!
    var interactor: CreateProfileInteractorInput!
    var router: CreateProfileRouterInput!
    
    private weak var output: CreateProfileModuleOutput?
    
    
    // MARK: - CreateProfileModuleInput
    
    func configureModule(delegate: CreateProfileModuleOutput) {
        output = delegate
    }
    


    // MARK: - CreateProfileViewOutput

    func viewIsReady() {
        view.setupInitialState()
    }
    
    func didTapCloseButton() {
        output?.moduleHasFinishedTask(self)
    }
    
    func didTapCreateButton(jobTitle: String, dayRate: Double?, annualSalary: Double?) {
        if let dayRate = dayRate {
            interactor.createProfile(jobTitle: jobTitle, dayRate: dayRate)
        }
        // TODO: implement annual salary
    }
    
    
    // MARK: - CreateProfileInteractorOutput
    
    func didSucceedCreateProfile() {
        output?.moduleHasFinishedTask(self)
    }
    
    func didFailCreateProfile() {
        // TODO: aici tre de facut ceva
    }
    
}
