//
//  BWCreateMeetingBWCreateMeetingInitializer.swift
//  Biwy
//
//  Created by Vlad Ionita on 15/11/2016.
//  Copyright © 2016 Biwy. All rights reserved.
//

import UIKit

class CreateMeetingModuleInitializer: NSObject {
    
    @IBOutlet weak var createMeetingViewController: CreateMeetingViewController!
    
    override func awakeFromNib() {
        let configurator = CreateMeetingModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: createMeetingViewController)
    }
    
}

